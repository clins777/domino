package com.cin.domino.testes;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;
import java.util.Timer;

import com.cin.domino.redes.connection.Connection;


public class ServidorSimplesUDP {
	

	public static int portaInicial = Connection.portaWelcomeSocket;
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		
//		DatagramSocket serverSocket = new DatagramSocket(portaInicial);
		DatagramSocket serverSocket = new DatagramSocket(Connection.portaWelcomeSocket);
		System.out.println(serverSocket.isConnected());
		while(true)
		{
			byte[] entrada = new byte[1024];
			DatagramPacket packet = new DatagramPacket(entrada, entrada.length);
			serverSocket.receive(packet);
			InetAddress address = packet.getAddress();
			String adString = new String(address.getAddress());
			String a = new String(packet.getData()).split("\n")[0];
			System.out.println(a);
			String ack = "ACK--localhost--" + Connection.portaWelcomeSocket + "--0--\n";
			DatagramPacket p = new DatagramPacket(ack.getBytes(), 0, ack.getBytes().length, address, 8032);
			serverSocket.send(p);
		}
	}

}
