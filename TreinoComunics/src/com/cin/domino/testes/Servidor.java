package com.cin.domino.testes;
import java.util.ArrayList;
import java.util.Scanner;

import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.MessageListener;
import com.cin.domino.redes.connectors.ConnectionReceiver;
import com.cin.domino.redes.connectors.Connector;
import com.cin.domino.redes.connectors.TCPConnector;
import com.cin.domino.redes.connectors.UDPConnector;



public class Servidor implements ConnectionReceiver, MessageListener{
	static int IDCounter = 0;
	ArrayList<Connection> conexoes;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int chanceDeletar = Integer.parseInt(in.nextLine());
		Servidor servidor = new Servidor();
//		Connector conector = new TCPConnector(1001, servidor);
		Connector conector = new UDPConnector(Connection.portaWelcomeSocket, servidor, chanceDeletar);
//		Connector conector = new UDPConnector(6666, servidor);
		servidor.conexoes = new ArrayList<Connection>();
		conector.start();
		while(true)
		{
			System.out.println("mensagem pra quem?");
			int x = Integer.parseInt(in.nextLine());
			System.out.println("digite a mensagem");
			servidor.conexoes.get(x).sendMessage(in.nextLine());
		}
	}

	@Override
	public void receiveMessage(String message, Connection connection) {
		System.out.println("Mensagem de " + connection.getID() + ": " + message);
	}

	@Override
	public void connectionFail(Connection connection) {
		System.out.println("Caiu a conexao "+connection.getID());
	}

	@Override
	public void receiveConnection(Connection connection, boolean hasStarted) {
		connection.setReceiver(this);
		connection.setID("Conexao "+IDCounter++);
		conexoes.add(connection);
		if(!hasStarted)
			connection.start();
	}

	@Override
	public void connectionOn(Connection connection) {
		// TODO Auto-generated method stub
		
	}

}
