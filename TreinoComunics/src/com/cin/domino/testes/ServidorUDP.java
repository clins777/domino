package com.cin.domino.testes;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;
import java.util.Timer;

import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.MessageListener;
import com.cin.domino.redes.connectors.ConnectionReceiver;
import com.cin.domino.redes.connectors.UDPConnector;


public class ServidorUDP implements ConnectionReceiver, MessageListener{
	

	public static int portaInicial = Connection.portaWelcomeSocket;
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		ServidorUDP serv = new ServidorUDP();
		
		int chanceDeletar = new Scanner(System.in).nextInt();
		UDPConnector connector = new UDPConnector(Connection.portaWelcomeSocket, serv, chanceDeletar);
		connector.start();
//		Scanner in = new Scanner(System.in);
//		in.next();
		
////		DatagramSocket serverSocket = new DatagramSocket(portaInicial);
//		DatagramSocket serverSocket = new DatagramSocket(Connection.portaWelcomeSocket);
//		while(true)
//		{
//			byte[] entrada = new byte[1024];
//			DatagramPacket packet = new DatagramPacket(entrada, entrada.length);
//			serverSocket.receive(packet);
//			InetAddress address = packet.getAddress();
//			String adString = new String(address.getAddress());
//			String a = new String(packet.getData()).split("\n")[0];
//			System.out.println(a);
//			String ack = "ACK--localhost--" + Connection.portaWelcomeSocket + "--0--\n";
//			DatagramPacket p = new DatagramPacket(ack.getBytes(), 0, ack.getBytes().length, address, 8032);
//			serverSocket.send(p);
//		}
	}
	@Override
	public void receiveMessage(String message, Connection connection) {
		System.out.println("mensagem recebida no final!: " + message);
	}
	@Override
	public void connectionFail(Connection connection) {
		System.out.println("connection Fail");
	}
	@Override
	public void connectionOn(Connection connection) {
		
	}
	@Override
	public void receiveConnection(Connection connection, boolean hasStarted) {
		connection.setReceiver(this);
	}

}
