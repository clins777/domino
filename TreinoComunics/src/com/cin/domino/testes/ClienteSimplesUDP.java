package com.cin.domino.testes;
import java.io.IOException;
import java.net.*;
import java.util.Scanner;

import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connectors.UDPConnector;
public class ClienteSimplesUDP {
	public static int porta = 4027;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String entrada = in.nextLine()+'\n';
		
		try {
//			DatagramSocket sock = new DatagramSocket(1002, InetAddress.getByName("192.168.1.6"));
			DatagramSocket sock = new DatagramSocket(porta);
			InetAddress IPServidor = InetAddress.getByName("localhost");
//			InetAddress IPServidor = InetAddress.getByName("192.168.1.7");
			
			DatagramPacket packet = null;
			byte[] entradaEmBytes = new byte[1024];
			entradaEmBytes = entrada.getBytes();
			
			packet = new DatagramPacket(entradaEmBytes, entradaEmBytes.length, IPServidor, ServidorUDP.portaInicial);
			sock.send(packet);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
