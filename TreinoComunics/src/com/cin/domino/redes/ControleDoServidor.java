package com.cin.domino.redes;

import java.util.Hashtable;

import com.cin.domino.game.logic.Sala;
import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.MessageListener;
import com.cin.domino.redes.connectors.ConnectionReceiver;
import com.cin.domino.redes.connectors.Connector;
import com.cin.domino.redes.connectors.TCPConnector;
import com.cin.domino.redes.connectors.UDPConnector;
import com.cin.domino.redes.mensagens.Mensagem;
import com.cin.domino.redes.mensagens.MensagemControleInicio;
import com.cin.domino.redes.mensagens.MensagemControleNomeInvalido;
import com.cin.domino.redes.mensagens.MensagemControleReconnect;
import com.cin.domino.redes.mensagens.MensagemControleSalaOff;


public class ControleDoServidor implements MessageListener, ConnectionReceiver{
	private static int porta = 1001;
	Hashtable<Integer, Sala> HashSalas;
	Hashtable<String, Connection> hashPlayers;
	Connector conector;
	private Sala salaIncompleta;
	float chanceDeletar;
	public ControleDoServidor(boolean TCP, int chanceDeletar)
	{
		
		if(TCP)
		{
			System.out.println("servidor TCP");
			conector = new TCPConnector(porta, this);
		}
		else
			conector = new UDPConnector(porta, this, chanceDeletar);
		conector.start();
		salaIncompleta = new Sala();
		HashSalas = new Hashtable<Integer, Sala>();
		hashPlayers = new Hashtable<String, Connection>();
	}
	public void receiveMessage(String message, Connection connection) {
		System.out.println("Mensagem recebida pelo controle do servidor: "+message);
		Mensagem mensagem = Mensagem.decodificaMensagem(message);
		if(mensagem instanceof MensagemControleInicio)
		{
			//lembrar de checar por nomes iguais!!!!!!!!!
			MensagemControleInicio decodificada = (MensagemControleInicio) mensagem;
			if(hashPlayers.containsKey(decodificada.nome))
			{
				Connection conexaoExistente = hashPlayers.get(decodificada.nome);
				if(!connection.getIP().equals(conexaoExistente.getIP()))
				{
					connection.sendMessage(MensagemControleNomeInvalido.createString());
				}
			} else
			//adcionar jogador retorna true se ap�s adcionar esse jogador a sala ficar completa e a partida inicia
			if(salaIncompleta.adcionarJogador(decodificada.nome, connection))
			{
				HashSalas.put(salaIncompleta.getIDSala(), salaIncompleta);
				salaIncompleta = new Sala();
			}
		}
		else if(mensagem instanceof MensagemControleReconnect)
		{
			System.out.println("reconnect!");
			MensagemControleReconnect decodificada = (MensagemControleReconnect) mensagem;
			Sala sala = HashSalas.get(decodificada.idSala);
			if(sala != null)
			{
				System.out.println("achou uma sala!");
				sala.pedidodeReconectar(decodificada.nome, connection);
			}
			else
			{
				System.out.println("nenhuma sala encontrada!");
				connection.sendMessage(MensagemControleSalaOff.createString());
			}
		}
	}
	
	@Override
	public void connectionFail(Connection connection) {
		// TODO Auto-generated method stub
		System.out.println("erro numa conex�o que ainda nao estava ligada a uma sala");
	}
	@Override
	public void connectionOn(Connection connection) {
		System.out.println("connection on");
	}
	@Override
	public void receiveConnection(Connection connection, boolean hasStarted) {
		connection.setReceiver(this);
		System.out.println("nova conexao 2");
		if(!hasStarted)
			connection.start();
	}
	
}
