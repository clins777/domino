package com.cin.domino.redes;

import java.util.Scanner;

public class ServidorDomino {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("tcp(0), ou udp (1)");
		int escolha = in.nextInt();
		int chanceDeletar = in.nextInt();
		ControleDoServidor controller = new ControleDoServidor(escolha == 0,  chanceDeletar);
	}

}
