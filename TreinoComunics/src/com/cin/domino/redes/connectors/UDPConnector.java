package com.cin.domino.redes.connectors;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Hashtable;

import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.MensagemUDP;
import com.cin.domino.redes.connection.MensagemUDP.tipoMensagem;
import com.cin.domino.redes.connection.MessageListener;
import com.cin.domino.redes.connection.UDPConnection;

public class UDPConnector extends Connector implements MessageListener{
	private class ID
	{
		public ID(int porta, String ip)
		{
			this.porta = porta;
			this.ip = ip;
		}
		int porta;
		String ip;
		public boolean equals(Object o)
		{	
			if(o == null)
			{
				return false;
			}
			if(!(o instanceof ID))
			{
				return false;
			}
			 boolean retorno = ((ID)o).porta == this.porta && ((ID)o).ip == this.ip;
			System.out.println(this.toString() + " = " + o.toString()+ " ->"+retorno);
			return ((ID)o).porta == this.porta && ((ID)o).ip == this.ip;
		}
		public String toString()
		{
			return "ID: "+ this.ip+ " porta: " + this.porta; 
		}
	}
	DatagramSocket welcomeSocket;
	Hashtable<ID, UDPConnection> conexoes;
	private int chanceDeletar;
	public UDPConnector(int port, ConnectionReceiver admin, int chanceDeletar) {
		super(port, admin);
		conexoes = new Hashtable<ID, UDPConnection>();
		this.chanceDeletar = chanceDeletar;
	}

	public void run()
	{

		try {
			welcomeSocket = new DatagramSocket(Connection.portaWelcomeSocket);
			while(true)
			{
				DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
				welcomeSocket.receive(packet);
				MensagemUDP syn = new MensagemUDP(packet.getData());
				if(syn.getTipo() == MensagemUDP.tipoMensagem.SYN)
				{
					int porta = getNewPort();
					UDPConnection conexao = conexoes.get(new ID(porta, syn.getIPOrigem()));
					if(conexao == null)
					{
						conexao = new UDPConnection(syn.getIPOrigem(), "NOID", syn.getPortaOrigem(), porta, this, chanceDeletar);
						conexoes.put(new ID(porta, syn.getIPOrigem()), conexao);
						conexao.start();
					}
					MensagemUDP ack = new MensagemUDP(conexao, "", tipoMensagem.ACK, syn.getPortaOrigem(), porta, syn.getIPOrigem(),meuIP);
					MensagemUDP synResposta = new MensagemUDP(conexao, "", tipoMensagem.ACK, syn.getPortaOrigem(), porta, syn.getIPOrigem(),meuIP);
					conexao.sendMessage(ack);
					conexao.sendMessage(synResposta);
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void receiveMessage(String message, Connection connection) {
		System.out.println("UDP conector recebeu mensagem \"" + message + "\", isto nao deveria acontecer");
	}

	@Override
	public void connectionFail(Connection connection) {
		System.out.println("conexao falhou");
	}

	@Override
	public synchronized void connectionOn(Connection connection) {
		admin.receiveConnection(connection, true);
	}
}
