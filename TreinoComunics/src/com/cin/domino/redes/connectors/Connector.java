package com.cin.domino.redes.connectors;

import java.net.InetAddress;
import java.net.UnknownHostException;


public abstract class Connector extends Thread {
	protected static int portCounter = 1201;
	protected int port;
	protected ConnectionReceiver admin;
	protected String meuIP;
	public Connector(int port, ConnectionReceiver admin)
	{
		this.port = port;
		this.admin = admin;
		try {
			meuIP = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	static int getNewPort()
	{
		return portCounter++;
	}
}
