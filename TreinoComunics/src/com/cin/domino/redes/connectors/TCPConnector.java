package com.cin.domino.redes.connectors;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.TCPConnection;



public class TCPConnector extends Connector {
	
	private ServerSocket welcomeSocket;
	public TCPConnector(int porta, ConnectionReceiver admin)
	{
		super(porta, admin);
	}
	public void run()
	{
		try {
			System.out.println("iniciou welcome socket");
			welcomeSocket = new ServerSocket(Connection.portaWelcomeSocket);
			while(true)
			{
				Socket socket = welcomeSocket.accept();
				System.out.println("aceitou socket");
				admin.receiveConnection(new TCPConnection(socket, "NOID", Connector.getNewPort(), null), false);
			}
		} catch (IOException e) {
//			e.printStackTrace();
		}
	}
}
