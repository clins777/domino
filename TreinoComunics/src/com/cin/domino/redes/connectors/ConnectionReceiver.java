package com.cin.domino.redes.connectors;

import com.cin.domino.redes.connection.Connection;
/**
 * 
 * Inteface que uma classe deve implementar para ser chamado por um
 *
 */
public interface ConnectionReceiver {
	void receiveConnection(Connection connection, boolean hasStarted);
}
