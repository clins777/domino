package com.cin.domino.redes.connectors;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.Timer;

import com.cin.domino.redes.connection.UDPConnection;


public class MensagemUDP implements ActionListener{
	private enum tipoMensagem
	{
		SYN, ACK, STD
	}

	private static final int tempoTemporizador = 500;
	private static final int maximoTentativas = 5;
	boolean confirmada;
	int tentativasRestantes;
	Timer temporizador;
	DatagramPacket pacote;
	String mensagem;
	tipoMensagem tipo;
	String IPOrigem;
	int portaOrigemMensagem;
	int contadorMensagem;
	UDPConnection conexao;
	DatagramSocket socket;

	public MensagemUDP(UDPConnection conexao, String mensagem, tipoMensagem tipo, int portaDestinoMensagem, int portaOrigem,String ipDestino,  String ipOrigem) {
		this.mensagem = mensagem;
		this.tipo = tipo;
		this.IPOrigem = ipOrigem;
		this.portaOrigemMensagem = portaOrigem;
		if(tipo == tipoMensagem.ACK)
			this.contadorMensagem = conexao.getContadorDestino();
		else
			this.contadorMensagem = conexao.getContador();
		tentativasRestantes = maximoTentativas;
		confirmada = false;
		temporizador = new Timer(tempoTemporizador, this);
		mensagem = tipo.toString() + "--" + ipOrigem + "--" + portaOrigem+ "--" + contadorMensagem  + "--" + mensagem;
		mensagem += '\n';
		try {
			pacote = new DatagramPacket(mensagem.getBytes(), mensagem.length(), InetAddress.getByName(ipDestino), portaDestinoMensagem);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public MensagemUDP(byte[] datagrama, UDPConnection conexao)
	{
		String[] mensagem = new String(datagrama).split("--");

		if("SYN".equals(mensagem[0]))
		{
			this.tipo = tipoMensagem.SYN; 
		} else if("ACK".equals(mensagem[0]))
		{
			this.tipo = tipoMensagem.ACK; 
		} else if("STD".equals(mensagem[0]))
		{
			this.tipo = tipoMensagem.STD; 
		}

		IPOrigem = mensagem[1];

		this.portaOrigemMensagem = Integer.parseInt(mensagem[2]);
		this.contadorMensagem = Integer.parseInt(mensagem[3]);

		this.mensagem = mensagem[4].split("\n")[0];
	}
	public void confirmar()
	{
		temporizador.stop();
		confirmada = true;
	}
	public void cancelar()
	{
		temporizador.stop();
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		temporizador.stop();
		tentativasRestantes--;
		if(tentativasRestantes == 0)
		{
			cancelar();
		}
		else
		{
			enviar();
		}
	}
	public void enviar()
	{
		try {
			socket.send(pacote);
			temporizador.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



}
