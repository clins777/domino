package com.cin.domino.game.logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import com.cin.domino.game.logic.Mao;
import com.cin.domino.game.logic.Peca;
import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.MessageListener;
import com.cin.domino.redes.mensagens.Mensagem;
import com.cin.domino.redes.mensagens.MensagemControleQuit;
import com.cin.domino.redes.mensagens.MensagemJogoEndGame;
import com.cin.domino.redes.mensagens.MensagemJogoEndPartida;
import com.cin.domino.redes.mensagens.MensagemJogoJogada;

public class Jogador implements MessageListener, ActionListener{
	private Mao mao;
	private String nome;
	private int maiorCarroca;
	private Peca maiorCarrocaPeca;
	private Connection conexao;
	private Sala sala;
	private int quantPecas;
	public boolean online;
	private Timer timer;

	public Jogador(String nome, Sala sala) {
		online=true;
		quantPecas=0;
		this.nome = nome;
		this.sala = sala;
		mao = new Mao();
		maiorCarroca = -1;
	}

	public void zerarMao(){
		mao = new Mao();
		maiorCarroca = -1;
		quantPecas = 0;
		
	}
	
	public void subQuantPecas() {
		quantPecas--;
	}

	public int getQuantPecas() {
		return quantPecas;
	}



	public void setQuantPecas(int quantPecas) {
		this.quantPecas = quantPecas;
	}



	public Connection getConexao() {
		return conexao;
	}



	public void setConexao(Connection conexao) {
		this.conexao = conexao;
	}



	public void setMao(Mao mao) {
		this.mao = mao;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getNome() {
		return nome;
	}	

	public Mao getMao() {
		return mao;
	}

	public void inserirPeca(Peca P) {
		mao.add(P);
		int x = P.carroca();
		if(x > maiorCarroca) {
			maiorCarroca = x;
			maiorCarrocaPeca = P;
		}
	}

	public int getMaiorCarroca() {
		return maiorCarroca;
	}

	public Peca getMaiorCarrocaPeca() {
		return maiorCarrocaPeca;
	}

	public Peca removerPeca(Peca P){
		Peca pecaRetorno = mao.remove(P);
		return pecaRetorno;
	}

	public Peca removerPeca(int index){
		Peca P = mao.get(index);
		mao.remove(index);
		return P;
	}
	public String toString()
	{  
		return this.nome +" tamanho da mao: " + this.mao.size() + "\n" +this.mao.toString();
	}

	@Override
	public void receiveMessage(String message, Connection connection) {
		Mensagem mensagem = Mensagem.decodificaMensagem(message);
		if(mensagem instanceof MensagemJogoJogada)
		{
			MensagemJogoJogada jogada = (MensagemJogoJogada) mensagem;
			Partida partida = this.sala.getPartida();
			///////////////////////////			
			if(partida != null)
			{
				if(partida.getJogadorAtual()==this){
					boolean acabouPartida = partida.jogarPeca(jogada.iDaPeca, jogada.jDaPeca, jogada.lado);
					boolean acabouJogo = partida.isFim();
					if(acabouPartida)
					{
						sala.getJogadores()[0].zerarMao();
						sala.getJogadores()[1].zerarMao();
						sala.getJogadores()[2].zerarMao();
						sala.getJogadores()[3].zerarMao();
						if(acabouJogo)
						{
							for (Jogador jogador : sala.getJogadores()) {
								int[] pontuacao = new int[2];
								Dupla[] duplas = sala.getPartida().getDuplas();
								for (int i = 0; i < 2; i++) {
									if(duplas[i].haveJogador(jogador))
										pontuacao[0] = duplas[i].getPontos();
									else
										pontuacao[1] = duplas[i].getPontos();
								}
								jogador.getConexao().sendMessage(MensagemJogoEndGame.createString(pontuacao));
							}
						}
						else if(partida.empate)
						{
							for (Jogador jogador : sala.getJogadores()) {
//								jogador.getConexao().sendMessage(Mensagem)
							}
						}else
						{
							for (Jogador jogador : sala.getJogadores()) {
								int[] pontuacao = new int[2];
								Dupla[] duplas = sala.getPartida().getDuplas();
								boolean ganhou = false;
								for (int i = 0; i < 2; i++) {
									if(duplas[i].haveJogador(jogador))
									{
										pontuacao[0] = duplas[i].getPontos();
										//
										ganhou = duplas[i].haveJogador(partida.getJogadorAtual());
									}
									else
										pontuacao[1] = duplas[i].getPontos();
								}
								jogador.getConexao().sendMessage(MensagemJogoEndPartida.createString(pontuacao, ganhou, partida.empate));
							}
							//tempo em ms
							System.out.println("iniciou timer pra nova partida");
							timer = new Timer(5000, this);
							timer.start();
						}
					}
				}
			}
			else if(mensagem instanceof MensagemControleQuit)
			{
				online = false;
				if(sala.getPartida()!= null)
					sala.getPartida().enviaStatus();
				else
					sala.removerJogador(this);
			}
			else
				System.out.println("algo errado, mandou mensagem de dentro da partida antes de estar na partida");
		}

	}



	@Override
	public void connectionFail(Connection connection) {
		online = false;
		if(sala.getPartida()!= null)
			sala.getPartida().enviaStatus();
		else
			sala.removerJogador(this);
	}

	@Override
	public void connectionOn(Connection connection) {
		// TODO Auto-generated method stub

	}
	// Isso e chamado ao final do timer.
	public void actionPerformed(ActionEvent e) {
		System.out.println("nova partida");
		Dupla dupla1 = sala.getPartida().getDuplas()[0];
		Dupla dupla2 = sala.getPartida().getDuplas()[1];
		sala.iniciaPartida(dupla1, dupla2);
		timer.stop();
		
	}
}
