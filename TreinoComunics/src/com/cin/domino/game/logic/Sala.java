package com.cin.domino.game.logic;

import java.util.ArrayList;

import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.mensagens.MensagemControleReconnect;
import com.cin.domino.redes.mensagens.MensagemControleSalaOff;
import com.cin.domino.redes.mensagens.MensagemJogadorWaitBegin;
import com.cin.domino.redes.mensagens.MensagemJogoState;


public class Sala {
	private Partida partida;
	private ArrayList<Jogador> jogadores;
	private int IDSala;
	private static int UltimoIDSala = 0;
	public Sala()
	{
		this.IDSala = gerarIDSala();
		jogadores = new ArrayList<Jogador>();
	}
	public boolean adcionarJogador(String nome, Connection conexao)
	{
		Jogador j = new Jogador(nome, this);
		conexao.setReceiver(j);
		j.setConexao(conexao);
		jogadores.add(j);
		for (Jogador jogador : jogadores) {
			if(jogador != null)
				jogador.getConexao().sendMessage(MensagemJogadorWaitBegin.createString(4-jogadores.size()));
		}
		if(jogadores.size() > 3)
		{
			iniciaPartida();
			return true;
		}
		return false;
	}
	public void removerJogador(Jogador jogador)
	{
		jogadores.remove(jogador);
		for (Jogador J : jogadores) {
			if(J != null)
				J.getConexao().sendMessage(MensagemJogadorWaitBegin.createString(4-jogadores.size()));
		}
	}
	private void iniciaPartida() {
		partida = new Partida(new Dupla(jogadores.get(0), jogadores.get(1)), new Dupla(jogadores.get(2), jogadores.get(3)), this);
		partida.iniciarPartida(6);
	}
	public void iniciaPartida(Dupla d1, Dupla d2) {
		partida = new Partida(d1, d2, this);
		partida.iniciarPartida(6);
	}
	public Partida getPartida() {
		return partida;
	}
	public int getIDSala()
	{
		return IDSala;
	}
	private static int gerarIDSala() {
		return UltimoIDSala++;
	}
	public Jogador[] getJogadores()
	{
		Jogador[] j = new Jogador[4];
		jogadores.toArray(j);
		return j;
	}
	public void pedidodeReconectar(String nome, Connection connection) {
		for (Jogador jogador : jogadores) {
			if(jogador.getNome().equals(nome) && !jogador.online)
			{
				jogador.setConexao(connection);
				connection.setReceiver(jogador);
				jogador.online = true;
				partida.enviaStatus();
				return;
			}
		}
		connection.sendMessage(MensagemControleSalaOff.createString());
	}
}
