package com.cin.domino.game.logic;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import com.cin.domino.redes.mensagens.MensagemJogoState;



public class Partida
{
	private static int pontosParaTerminarJogo = 6;
	private Tabuleiro tabuleiro;
	private Dupla duplas[];
	private Jogador jogadorAtual;
	private Jogador proximoJogador;
	private Dupla duplaVencedora;
	/**
	 * localiza��o do jogador atual, [0] � a posi��o em dupals e [1] � a posi��o no array da dupla
	 */
	private int ij [];
	private boolean fim;
	private Scanner teclado = new Scanner(System.in);
	private Jogador primeiroJogador;
	private Sala sala;
	boolean empate;


	public Partida (Dupla dupla1, Dupla dupla2, Sala sala) {
		this.sala = sala;
		duplas = new Dupla[2];
		duplas[0] = dupla1;
		duplas[1] = dupla2;
	}
	public Dupla getDuplaVencedora(){
		Dupla duplaV;
		if(fim)
			duplaV = getDuplaDaVez();
		else if(duplas[0].contarPecas()<duplas[1].contarPecas())
			duplaV = duplas[1];
		else
			duplaV = duplas[0];
		return duplaV;
	}

	public Dupla getDuplaDaVez(){
		return duplas[ij[1]];
	}

	public Dupla[] getDuplas() {
		return duplas;
	}

	public Jogador getPrimeiroJogador() {
		return primeiroJogador;
	}
	public boolean isFim() {
		return fim;
	}

	public void iniciarPartida(int quantidadeDePecasNaMao){
		empate = false;
		duplas[0].getDupla()[0].setQuantPecas(quantidadeDePecasNaMao);
		duplas[0].getDupla()[1].setQuantPecas(quantidadeDePecasNaMao);
		duplas[1].getDupla()[0].setQuantPecas(quantidadeDePecasNaMao);
		duplas[1].getDupla()[1].setQuantPecas(quantidadeDePecasNaMao);
		fim = false;
		for(int i=0; i<2; i++){
			for(int j=0; j<2; j++) {				
				duplas[j].getDupla()[i].getMao().clear();
			}
		}
		Peca pecas[] = new Peca[28];
		int aux = 0;
		if(quantidadeDePecasNaMao>7) 
			quantidadeDePecasNaMao = 7;
		for(int i=0; i<7; i++) {
			for(int j=0; j<=i; j++) {
				pecas[aux] = new Peca(i,j);
				aux++;
			}
		}
		aux--;
		int fim = 27;
		int random;
		int contAUX=0;
		for(int i=0; i<quantidadeDePecasNaMao; i++){

			//jogador 0 dupla 0
			random = (int)(Math.random() * aux);
			duplas[0].getDupla()[0].inserirPeca( pecas[random]);
			pecas[random] = pecas[fim];
			fim--;
			aux--;

			//jogador 0 dupla 1
			random = (int)(Math.random() * aux);
			duplas[1].getDupla()[0].inserirPeca( pecas[random]);
			pecas[random] = pecas[fim];
			fim--;
			aux--;

			//jogador 1 dupla 0
			random = (int)(Math.random() * aux);
			duplas[0].getDupla()[1].inserirPeca( pecas[random]);
			pecas[random] = pecas[fim];
			fim--;
			aux--;

			//jogador 1 dupla 1
			random = (int)(Math.random() * aux);
			duplas[1].getDupla()[1].inserirPeca( pecas[random]);
			pecas[random] = pecas[fim];
			fim--;
			aux--;

			contAUX++;
		}

		ij = new int [2];
		int maior=-1;
		//gambiarra para varrer todos jogadores.
		for(int i=0; i<2; i++){
			for(int j=0; j<2; j++) {
				if(duplas[j].getDupla()[i].getMaiorCarroca()>maior) {
					ij[0] = i;
					ij[1] = j;
					maior = duplas[j].getDupla()[i].getMaiorCarroca();
				}
			}
		}
		jogadorAtual = duplas[ij[1]].getDupla()[ij[0]];
		primeiroJogador = jogadorAtual;
		calcNext();
		Peca peca = jogadorAtual.getMaiorCarrocaPeca();
		jogadorAtual.getMao().remove(peca);
		jogadorAtual.getMao().remove(peca);
		tabuleiro = new Tabuleiro(peca);
		jogadorAtual.subQuantPecas();
		jogadorAtual = proximoJogador;
		calcNext();
		boolean sai = false;
		for(int k=0; k<6 && !sai; k++){
			boolean toque = true;
			for (Peca p : jogadorAtual.getMao()) {
				if(podeColocarPeca(p, true) || podeColocarPeca(p, false)) toque=false;
			}

			if(toque) {
				System.out.println("Voce tocou, Imbecil!");
				jogadorAtual = proximoJogador;
				calcNext();
			}else{
				//				//qual peca o jogador vai jogar?
				//				System.out.println("Qual peca vc quer jogar? (apartir de 1)");
				//				System.out.println("Qual lado? (1 para esquerda e 2 para direita)");
				sai = true;
			}
		}
		enviaStatus();
	}

	private void calcNext(){
		/*if(ij[1]==0){
			ij[1]=1;
		}else{			
			if(ij[0]==0) ij[0]=1;
			else ij[0]=0;
			ij[1]=0;
		}*/
		if(ij[1] == 1)
			ij[0] = (ij[0]+1) %2;
		ij[1] = (ij[1]+1)%2;
		proximoJogador = duplas[ij[1]].getDupla()[ij[0]];
	}

	public Jogador[] ProxJogs(Jogador J){
		/*if(ij[1]==0){
			ij[1]=1;
		}else{			
			if(ij[0]==0) ij[0]=1;
			else ij[0]=0;
			ij[1]=0;
		}*/
		int aux[] = {ij[0], ij[1]} ;
		for(int i=0; i<4 && J!=duplas[ij[1]].getDupla()[ij[0]]; i++){
			if(ij[1] == 1)
				ij[0] = (ij[0]+1) %2;
			ij[1] = (ij[1]+1)%2;
		}
		Jogador proximosJogadores [] = new Jogador[3];
		for(int i=0; i<3; i++){
			if(ij[1] == 1)
				ij[0] = (ij[0]+1) %2;
			ij[1] = (ij[1]+1)%2;
			proximosJogadores [i] = duplas[ij[1]].getDupla()[ij[0]];
		}
		ij = aux;
		return proximosJogadores;
	}

	public Jogador getJogadorAtual() {
		return jogadorAtual;
	}

	public Jogador getProximoJogador() {
		return proximoJogador;
	}

	public boolean podeColocarPeca(Peca peca, boolean lado){
		Peca ponta;
		if(lado) ponta = tabuleiro.get(tabuleiro.size()-1);
		else ponta = tabuleiro.get(0);
		if(ponta.getVizinhos()[0] == null) {
			if(peca.getNumeros()[0]==ponta.getNumeros()[0] || peca.getNumeros()[1]==ponta.getNumeros()[0])
				return true;
			else
				return false;
		} else {
			if(peca.getNumeros()[0]==ponta.getNumeros()[1] || peca.getNumeros()[1]==ponta.getNumeros()[1])
				return true;
			else
				return false;
		}
	}

	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public boolean jogarPeca(int i, int j, boolean lado){

		jogadorAtual.subQuantPecas();
		//////////////////
		Peca peca = this.jogadorAtual.getMao().remove(new Peca(i,j));
		///////////////////
		Peca ponta;
		int size = tabuleiro.size();
		int L;
		if(lado){
			ponta = tabuleiro.get(size-1);
			L = size;
		}
		else {
			ponta = tabuleiro.get(0);
			L = 0;
		}
		if(jogadorAtual.getMao().size()==0) {
			if(sala.getPartida().getDuplaDaVez().getPontos()+pontosGanho(peca) >= pontosParaTerminarJogo){
				if(duplas[0].haveJogador(jogadorAtual))
					duplaVencedora = duplas[0];
				else
					duplaVencedora = duplas[1];
				fim = true;
				return true;
			}
			System.out.println("O jogador Atual ("+ jogadorAtual.getNome() +") Bateu.");
			for(int k=0; k<2; k++){
				if(duplas[k].haveJogador(jogadorAtual))
					duplas[k].adicionarPontos(pontosGanho(peca));
			}
			tabuleiro.add(L, peca);	
			enviaStatus();
			return true;
		}

		if(ponta.getVizinhos()[0] == null) {
			if(peca.getNumeros()[0]==ponta.getNumeros()[0]) {
				peca.getVizinhos()[0]=ponta;
				ponta.getVizinhos()[0]=peca;
			}
			else {
				peca.getVizinhos()[1]=ponta;
				ponta.getVizinhos()[0]=peca;
			}
		} else {
			if(peca.getNumeros()[0]==ponta.getNumeros()[1]) {
				peca.getVizinhos()[0]=ponta;
				ponta.getVizinhos()[1]=peca;
			}
			else {
				peca.getVizinhos()[1]=ponta;
				ponta.getVizinhos()[1]=peca;
			}
		}
		Jogador JAux = jogadorAtual;
		jogadorAtual = proximoJogador;
		calcNext();
		boolean sai = false;
		tabuleiro.add(L, peca);
		for(int k=0; k<5 && !sai; k++){
			boolean toque = true;
			for (Peca p : jogadorAtual.getMao()) {
				if(podeColocarPeca(p, true) || podeColocarPeca(p, false)) toque=false;
			}

			if(toque) {
				System.out.println("Voce tocou, Imbecil!");
				jogadorAtual = proximoJogador;
				calcNext();
			}else{
				//				//qual peca o jogador vai jogar?
				//				System.out.println("Qual peca vc quer jogar? (apartir de 1)");
				//				System.out.println("Qual lado? (1 para esquerda e 2 para direita)");
				sai = true;
			}
		}
		if(sai){
			enviaStatus();
			return false;
		}
		else{
			jogadorAtual = JAux;
			if(duplas[0].contarPecas()==duplas[0].contarPecas()){
				empate = true;
				duplaVencedora = null;
			}
			else if(duplas[0].contarPecas()<duplas[1].contarPecas()){
				duplaVencedora = duplas[0];
				duplas[0].adicionarPontos(1);
			}
			else{
				duplaVencedora = duplas[1];
				duplas[1].adicionarPontos(1);
			}
			enviaStatus();
			return true;
		}
	}



	public int pontosGanho (Peca P){
		if(P == null)
			return 1;
		Peca aux1, aux2;
		aux1 = new Peca(P.getNumeros()[0],P.getNumeros()[0]);
		aux2 = new Peca(P.getNumeros()[1],P.getNumeros()[1]);		
		if(podeColocarPeca(P, true) && podeColocarPeca(P, false) && P.carroca()!=-1){
			return 4;
		} else if((podeColocarPeca(aux1, true) && podeColocarPeca(aux2, false)) || (podeColocarPeca(aux2, true) && podeColocarPeca(aux1, false))){
			return 3;
		} else if(P.carroca() != -1){
			return 2;
		} else {
			return 1;
		}
	}

	public String toString()
	{
		return "Tabuleiro: \n" + tabuleiro.toString()+"\n Dupla1: \n"  + duplas[0].toString() + "\n Dupla 2: \n" + duplas[1].toString();
	}
	public void enviaStatus(Jogador jogador)
	{
		if(jogador.online == true)
		{
			Jogador[] jogadoresOrdenados = ProxJogs(jogador);
			System.out.println(jogadoresOrdenados.toString());
			String[] nomes = {
					jogadoresOrdenados[0].getNome(),
					jogadoresOrdenados[1].getNome(),
					jogadoresOrdenados[2].getNome()};
			int[] maoQtd = {
					jogadoresOrdenados[0].getQuantPecas(),
					jogadoresOrdenados[1].getQuantPecas(),
					jogadoresOrdenados[2].getQuantPecas()};
			boolean[] jogadoresStatus = {
					jogadoresOrdenados[0].online,
					jogadoresOrdenados[1].online,
					jogadoresOrdenados[2].online};
			int[] pontos = new int[2];
			for (int i = 0; i < 2; i++) {
				if(duplas[i].haveJogador(jogador))
					pontos[0] = duplas[i].getPontos();
				else
					pontos[1] = duplas[i].getPontos();
			}
			Peca[] mao = new Peca[jogador.getMao().size()];
			jogador.getMao().toArray(mao);
			jogador.getConexao().sendMessage(MensagemJogoState.createString(nomes,maoQtd, jogadoresStatus, jogadorAtual.getNome(), tabuleiro, mao , pontos, sala.getIDSala(), jogador == jogadorAtual)); 
		}
		else
			System.out.println("jogador off, nao envia");
	}
	public void enviaStatus()
	{
		for (Jogador jogador : sala.getJogadores()) {
			enviaStatus(jogador); 
		}
	}

}
