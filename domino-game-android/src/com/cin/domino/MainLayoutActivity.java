package com.cin.domino;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainLayoutActivity extends Activity
{
	List<String> mRooms;
	List<String> mUsers;
	RoomAdapter mRoomAdapter;
	UserAdapter mUserAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mRooms = new ArrayList<String>();
		mUsers = new ArrayList<String>();
		mRoomAdapter = new RoomAdapter(getApplicationContext(), R.layout.list_inner_view, mRooms);
		mUserAdapter = new UserAdapter(getApplicationContext(), R.layout.list_inner_view, mUsers);
		ListView mRoomList = (ListView) findViewById(R.id.rooms_list);
		ListView mUserList = (ListView) findViewById(R.id.users_list);
		mRoomList.setAdapter(mRoomAdapter);
		mUserList.setAdapter(mUserAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(Menu.NONE, Menu.FIRST + 1, Menu.NONE, "").setIcon(R.drawable.suicide_icon);
		menu.add(Menu.NONE, Menu.FIRST + 2, Menu.NONE, "").setIcon(R.drawable.boom_icon);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case (Menu.FIRST + 1):
			startActivity(new Intent(MainLayoutActivity.this, MainActivity.class));
			break;
		case (Menu.FIRST + 2):
			startActivity(new Intent(MainLayoutActivity.this, ConnectTest.class));
//			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void addRoom(View v)
	{
		EditText mEt = (EditText) findViewById(R.id.room_input);
		String mNewRoom = mEt.getText().toString();
		if(mNewRoom.equalsIgnoreCase(""))
		{
			Toast.makeText(getApplicationContext(), "Seu noob", Toast.LENGTH_SHORT).show();
		} else
		{
			mEt.setText("");
			mRoomAdapter.add(mNewRoom);
		}
	}

	public void addUser(View v)
	{
		EditText mEt = (EditText) findViewById(R.id.user_input);
		String mNewUser = mEt.getText().toString();
		if(mNewUser.equalsIgnoreCase(""))
		{
			Toast.makeText(getApplicationContext(), "Seu noob", Toast.LENGTH_SHORT).show();
		} else
		{
			mEt.setText("");
			mUserAdapter.add(mNewUser);
		}
	}

	private class RoomAdapter extends ArrayAdapter<String>
	{
		Context context;
		List<String> roomList = new ArrayList<String>();
		int layoutResourceId;

		public RoomAdapter(Context context, int layoutResourceId, List<String> objects)
		{
			super(context, layoutResourceId, objects);
			this.roomList = objects;
			this.layoutResourceId = layoutResourceId;
			this.context = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			CheckBox checkBox = null;
			if(convertView == null)
			{
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.list_inner_view, parent, false);
				checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
				convertView.setTag(checkBox);
			} else
			{
				checkBox = (CheckBox) convertView.getTag();
			}
			String current = roomList.get(position);
			checkBox.setText(current);
			checkBox.setTag(current);
			return convertView;
		}
	}

	private class UserAdapter extends ArrayAdapter<String>
	{
		Context context;
		List<String> userList = new ArrayList<String>();
		int layoutResourceId;

		public UserAdapter(Context context, int layoutResourceId, List<String> objects)
		{
			super(context, layoutResourceId, objects);
			this.userList = objects;
			this.layoutResourceId = layoutResourceId;
			this.context = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			CheckBox checkBox = null;
			if(convertView == null)
			{
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.list_inner_view, parent, false);
				checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
				convertView.setTag(checkBox);
			} else
			{
				checkBox = (CheckBox) convertView.getTag();
			}
			String current = userList.get(position);
			checkBox.setText(current);
			checkBox.setTag(current);
			return convertView;
		}
	}


}
