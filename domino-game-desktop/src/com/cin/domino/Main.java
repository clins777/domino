package com.cin.domino;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration(); 
		cfg.title = "domino-game";
		cfg.useGL20 = false;
		cfg.width = 900;
		cfg.height = 700;
		cfg.resizable = false;
		//cfg.fullscreen = true;
		
		new LwjglApplication(new DominoGdx(), cfg);	
	}
}