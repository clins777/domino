package com.cin.domino.redes.connection;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.cin.domino.redes.mensagens.Mensagem;


public abstract class Connection extends Thread{
	public static int portaWelcomeSocket = 8532;
	protected String IP;
	protected String ID;
//	protected int port;
	protected MessageListener listener;
	protected boolean online;
	protected String meuIP;
	
	public Connection(String IP, String ID, MessageListener receiver)
	{
		this.IP = IP;
		this.ID = ID;
//		this.port = port;
		this.listener = receiver;
		try {
			meuIP = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public abstract void sendMessage(String message);
	
	public void sendMessage(Mensagem message)
	{
		sendMessage(message.toString());
	}
	
	public void setReceiver(MessageListener receiver)
	{
		this.listener = receiver;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getIP() {
		return IP;
	}
//	public int getPort() {
//		return port;
//	}
	public boolean isOnline() {
		return online;
	}
	public abstract void fechar();
}
