package com.cin.domino.redes.connection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.Timer;


public class MensagemUDP implements ActionListener
{
	public enum tipoMensagem
	{
		SYN, ACK, STD
	}

	private static final int tempoTemporizador = 2500;
	private static final int maximoTentativas = 4;
	boolean confirmada;
	int tentativasRestantes;
	Timer temporizador;
	DatagramPacket pacote;
	String mensagem;
	tipoMensagem tipo;
	String IPOrigem;
	public String getIPOrigem()
	{
		return IPOrigem;
	}
	int portaOrigemMensagem;
	public int getPortaOrigem()
	{
		return portaOrigemMensagem;
	}
	int contadorMensagem;
	UDPConnection conexao;
//	DatagramSocket socket;
	public tipoMensagem getTipo()
	{
		return this.tipo;
	}

	public MensagemUDP(UDPConnection conexao, String mensagem, tipoMensagem tipo, int portaDestinoMensagem, int portaOrigem,String ipDestino,  String ipOrigem) {
		this.tipo = tipo;
		this.IPOrigem = ipOrigem;
		this.portaOrigemMensagem = portaOrigem;
		this.conexao = conexao;
		confirmada = false;
		if(tipo == tipoMensagem.ACK)
		{
			confirmada = true;
			this.contadorMensagem = conexao.getContadorDestino();
		}
		else
			this.contadorMensagem = conexao.getContador();
		tentativasRestantes = maximoTentativas;
		temporizador = new Timer(tempoTemporizador, this);
		mensagem = tipo.toString() + "--" + ipOrigem + "--" + portaOrigem+ "--" + contadorMensagem  + "--" + mensagem;
		mensagem += '\n';
		this.mensagem = mensagem;
		try {
			pacote = new DatagramPacket(mensagem.getBytes(), mensagem.length(), InetAddress.getByName(ipDestino), portaDestinoMensagem);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public MensagemUDP(byte[] datagrama)
	{
		String[] mensagem = new String(datagrama).split("--");

		if("SYN".equals(mensagem[0]))
		{
			this.tipo = tipoMensagem.SYN; 
		} else if("ACK".equals(mensagem[0]))
		{
			this.tipo = tipoMensagem.ACK; 
		} else if("STD".equals(mensagem[0]))
		{
			this.tipo = tipoMensagem.STD; 
		}

		IPOrigem = mensagem[1];

		this.portaOrigemMensagem = Integer.parseInt(mensagem[2]);
		this.contadorMensagem = Integer.parseInt(mensagem[3]);

		this.mensagem = mensagem[4].split("\n")[0];
	}
	public void confirmar()
	{
		temporizador.stop();
		System.out.println("mensagem confirmada: " + this.tipo.toString() + this.mensagem);
		confirmada = true;
	}
	public void cancelar()
	{
		temporizador.stop();
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		temporizador.stop();
		if(!this.confirmada)
		{
			tentativasRestantes--;
	//		System.out.println("temporizador estourou, tentativas restantes = "+ tentativasRestantes);
			if(tentativasRestantes == 0)
			{
				conexao.cancelar();
			}
			else
			{
				enviar();
			}
		}
	}
	public synchronized void enviar()
	{
		try {
			if(conexao.getSocket()!= null)
			{
				int f = UDPConnection.random.nextInt(100);
				if(f > conexao.getChanceDeletar())
				{
					conexao.getSocket().send(pacote);
				}else
					System.out.println("deletou "+f+"-" + conexao.getChanceDeletar());
				System.out.println("mensagem enviada: "+ this.tipo.toString() + this.mensagem);
				if(tipo != tipoMensagem.ACK)
				{
					System.out.println("iniciou temporizador");
					temporizador.start();
				}
			}
			else
			{
				System.out.println("socket nulo!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
