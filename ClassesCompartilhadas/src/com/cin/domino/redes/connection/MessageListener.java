package com.cin.domino.redes.connection;


public interface MessageListener {
	public void receiveMessage(String message, Connection connection);
	public void connectionFail(Connection connection);
	public void connectionOn(Connection connection);
}
