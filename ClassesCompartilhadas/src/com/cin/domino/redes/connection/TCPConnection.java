package com.cin.domino.redes.connection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

import javax.swing.Timer;


public class TCPConnection extends Connection implements ActionListener{
	private Socket socket;
	private DataOutputStream OStream;
	private BufferedReader IReader;
	private int port;
	private Timer timerEnvio;
	private static int tempoEnvio = 1500;
	private static int maxChances = 4;
	private int chances;
	public TCPConnection(String IP, String ID, int port, MessageListener receiver)
	{
		super(IP, ID, receiver);
		this.port = port;
	}
	public TCPConnection(Socket socket, String ID, int port, MessageListener receiver)
	{
		super(socket.getInetAddress().getCanonicalHostName(),ID , receiver);
		this.port = port;
		this.socket = socket;
	}
	public void run()
	{
		try {
			if(socket == null)
				socket = new Socket(IP, Connection.portaWelcomeSocket);
			//				socket = new Socket(IP, port);
			//			socket.setSoTimeout(1000);
			OStream = new DataOutputStream(socket.getOutputStream());
			IReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.online = true;
			listener.connectionOn(this);
			timerEnvio = new Timer(tempoEnvio, this);
			timerEnvio.start();
			while(online)
			{
				socket.getOutputStream();
				String a = IReader.readLine();
				if(!("checagem".equals(a)))
					System.out.println("mensagem recebida: "+a);
				if(a != null)
				{
					if(!"checagem".equals(a))
						listener.receiveMessage(a, this);
				}
				else
					this.fechar();
				chances = maxChances;
				//timerEnvio.restart();
			}
		} catch (IOException e) {

			System.out.println("connection fail");
			listener.connectionFail(this);
			e.printStackTrace();
			this.online = false;
		}
	}
	public synchronized void sendMessage(String message){
		try {
			if(this.online)
			{
				if(!("checagem".equals(message)))
					System.out.println("mandou mensagem: " + message);
				OStream.writeBytes(message + '\n');
			}
			else
				listener.connectionFail(this);
		} catch (IOException e) {
			listener.connectionFail(this);
		}
	}
	public void fechar()
	{
		online = false;
		listener.connectionFail(this);
//		try {
//			socket.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.sendMessage("checagem");
		chances--;
//		System.out.println(chances);
		if(chances ==0)
		{
			timerEnvio.stop();
////			try {
////				socket.close();
				online = false;
				listener.connectionFail(this);
////			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}//////
		}
	}
}
