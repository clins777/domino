package com.cin.domino.redes.connection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.BindException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.Timer;

import com.cin.domino.redes.connection.MensagemUDP.tipoMensagem;
import com.cin.domino.redes.mensagens.Mensagem;


public class UDPConnection extends Connection implements ActionListener {

	ArrayList<MensagemUDP> bufferMensagens;
	MensagemUDP mensagemAtual;
	DatagramSocket socket;
	int contador;
	int contadorDestino;
	int portaDestino;
	int portaOrigem;
	boolean sabePortaDestino;
	Timer timerSyn;
	private int chanceDeletar;
	public static Random random;
	public int getChanceDeletar()
	{
		return chanceDeletar;
	}
	private static final int synInterval = 10000;
	public UDPConnection(String IPDestino, String ID, int portaDestino,
			int portaOrigem, MessageListener receiver, int chanceDeletar) {
		super(IPDestino, ID, receiver);
		bufferMensagens = new ArrayList<MensagemUDP>();
		contador = 0;
		this.portaDestino = portaDestino; 
		this.portaOrigem = portaOrigem;
		sabePortaDestino = true;
		timerSyn = new Timer(synInterval, this);
		this.chanceDeletar = chanceDeletar;
		random = new Random();
	}
	public UDPConnection(String IPDestino, String ID,
			int portaOrigem, MessageListener receiver) {
		super(IPDestino, ID, receiver);
		bufferMensagens = new ArrayList<MensagemUDP>();
		contador = 0;
		this.portaOrigem = portaOrigem;
		sabePortaDestino = false;
		timerSyn = new Timer(synInterval, this);
		chanceDeletar = 0;
		random = new Random();
	}
	public synchronized void criaSocket()
	{
		try {
			socket = new DatagramSocket(portaOrigem);
			System.out.println("criou o socket!");
		} catch (SocketException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			if(e instanceof BindException)
				portaOrigem++;
		}
	}
	public void run()
	{
//		while(socket == null)
//		{
			criaSocket();
//		}
		while(!sabePortaDestino)
		{
//			mensagemAtual = new MensagemUDP(this, "", MensagemUDP.tipoMensagem.SYN, Connection.portaWelcomeSocket);
			mensagemAtual = new MensagemUDP(this, "", MensagemUDP.tipoMensagem.SYN, Connection.portaWelcomeSocket, portaOrigem, IP, meuIP);
			mensagemAtual.enviar();
			DatagramPacket p = new DatagramPacket(new byte[1024], 1024);
			try {
				socket.receive(p);
				int f = random.nextInt(100);
				if(f > chanceDeletar)
				{
					MensagemUDP mensagem = new MensagemUDP(p.getData());
					if(mensagem.tipo == MensagemUDP.tipoMensagem.ACK)
					{
						IP = mensagem.IPOrigem;
						portaDestino = mensagem.portaOrigemMensagem;
						sabePortaDestino = true;
	//					contadorDestino = mensagem.contadorMensagem+1;
					}
				}
				else
					System.out.println("deletou "+f+"-" + chanceDeletar);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		online = true;
		listener.connectionOn(this);
		timerSyn.start();
		System.out.println("conexao iniciada");
		while(online)
		{
			DatagramPacket p = new DatagramPacket(new byte[1024], 1024);
			try {
				int f =(int) random.nextInt(100);
				if(f > chanceDeletar)
				{
					socket.receive(p);
					MensagemUDP datagrama = new MensagemUDP(p.getData());
					System.out.println("mensagem recebida: " + new String(p.getData()));
					String mensagem = mensagemRecebida(datagrama);
					if(mensagem != null)
						listener.receiveMessage(mensagem, this);
				}else
					System.out.println("deletou "+f+"-" + chanceDeletar);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * 
	 * @param mensagem a mensagem recebida
	 * @return um ack, ou null caso a mensagem nao necessite um ack
	 */
	public synchronized String mensagemRecebida(MensagemUDP mensagem)
	{
		switch (mensagem.tipo) {
		case ACK:
			if(mensagem.contadorMensagem >= mensagemAtual.contadorMensagem)
			{
				this.mensagemAtual.confirmar();
				if(!bufferMensagens.isEmpty())
				{
					mensagemAtual = bufferMensagens.get(0);
					bufferMensagens.remove(0);
					mensagemAtual.enviar();
				}
				return null;
			}
			break;
		case STD:
			if(mensagem.contadorMensagem == contadorDestino)
			{
//				MensagemUDP ack = new MensagemUDP(this, "", MensagemUDP.tipoMensagem.ACK, portaDestino);
				MensagemUDP ack = new MensagemUDP(this, "", MensagemUDP.tipoMensagem.ACK, portaDestino, portaOrigem, IP, meuIP);
				ack.enviar();
				contadorDestino++;
				return mensagem.mensagem;
			}
			break;
		case SYN:
//			if(mensagem.contadorMensagem == contadorDestino)
//			{
//				MensagemUDP ack = new MensagemUDP(this, "", MensagemUDP.tipoMensagem.ACK, portaDestino);
				MensagemUDP ack = new MensagemUDP(this, "", MensagemUDP.tipoMensagem.ACK, portaDestino, portaOrigem, IP, meuIP);
				ack.enviar();
//				contadorDestino++;
				return null;
//			}
		default:
			break;
		}
		return null;
	}
	@Override
	public void sendMessage(String message) {
//		MensagemUDP m  = new MensagemUDP(this, message, MensagemUDP.tipoMensagem.STD, this.portaDestino);
		MensagemUDP m = new MensagemUDP(this, message, MensagemUDP.tipoMensagem.STD, this.portaDestino, this.portaOrigem, IP, meuIP);
		contador++;
		sendMessage(m);
	}
	public synchronized void sendMessage(MensagemUDP m)
	{
		if(m.tipo == tipoMensagem.ACK)
		{
			m.enviar();
		}
		if(mensagemAtual == null || mensagemAtual.confirmada)
		{
			mensagemAtual = m;
			m.enviar();
		}
		else
		{
			bufferMensagens.add(m);
		}
	}
	//trigger do timer
	@Override
	public void actionPerformed(ActionEvent e) {
		if(mensagemAtual.confirmada == true && bufferMensagens.isEmpty())
		{
			mensagemAtual = new MensagemUDP(this, "", MensagemUDP.tipoMensagem.SYN, portaDestino, this.portaOrigem, IP, meuIP);
			mensagemAtual.enviar();
		}
	}

	public void cancelar() {
		online = false;
//		socket.close();
		listener.connectionFail(this);
	}
	public void fechar()
	{
		online = false;
		socket.close();
	}
	public int getContadorDestino() {
		return contadorDestino;
	}
	public int getContador()
	{
		return contador;
	}

	public DatagramSocket getSocket()
	{
		return this.socket;
	}
}
