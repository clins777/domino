package com.cin.domino.redes.mensagens;

public class Mensagem{
	public Mensagem(){}
	public static Mensagem decodificaMensagem(String mensagem)
	{
		if(mensagem == null)
			return null;
		String[] mensagemDividida = mensagem.split(":");
		String[] flags = mensagemDividida[0].split("[.]");
		if("jogo".equals(flags[0]))
		{
			if("jogada".equals(flags[1]))
			{
				return new MensagemJogoJogada(mensagemDividida[1]);
			}
			else if("state".equals(flags[1]))
			{
				return new MensagemJogoState(mensagemDividida[1]);
			}
			else if("endPartida".equals(flags[1]))
			{
				return new MensagemJogoEndPartida(mensagemDividida[1]);
			}
			else if("endGame".equals(flags[1]))
			{
				return new MensagemJogoEndGame(mensagemDividida[1]);
			}
		}
		else if("controle".equals(flags[0]))
		{
			if("inicio".equals(flags[1]))
			{
				return new MensagemControleInicio(mensagemDividida[1]);
			}
			else if("reconnect".equals(flags[1]))
			{
				return new MensagemControleReconnect(mensagemDividida[1]);
			}
			else if("salaOff".equals(flags[1]))
			{
				return new MensagemControleSalaOff();
			}
			else if("jogadorWaitBegin".equals(flags[1])){
				return new MensagemJogadorWaitBegin(mensagemDividida[1]);
			}
			else if("nomeInvalido".equals(flags[1])){
				return new MensagemControleNomeInvalido();
			}
			else if ("quit".equals(flags[1]))
			{
				return new MensagemControleQuit();
			}
		}
		return null;
	}
}
