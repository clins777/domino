package com.cin.domino.redes.mensagens;

public class MensagemJogadorWaitBegin extends Mensagem{
	public int playersRemain;
	
	public MensagemJogadorWaitBegin(String dados){
		this.playersRemain = Integer.parseInt(dados);
	}
	public static String createString(int playersRemain){
		return "controle.jogadorWaitBegin:"+playersRemain;
	}
}
