package com.cin.domino.redes.mensagens;

public class MensagemJogoEndGame extends Mensagem{
	public int[] pontuacao;
	
	public MensagemJogoEndGame(String dados){
		String[] array = dados.split(";");
		this.pontuacao = new int[2];
		this.pontuacao[0] = Integer.parseInt(array[0]);
		this.pontuacao[1] = Integer.parseInt(array[1]);
	}
	public static String createString(int[] potuacao){
		return "jogo.endGame:"+potuacao[0]+";"+potuacao[1];
	}
}
