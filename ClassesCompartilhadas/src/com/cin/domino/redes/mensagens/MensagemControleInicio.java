package com.cin.domino.redes.mensagens;

public class MensagemControleInicio extends Mensagem{
	public String nome;
	
	public MensagemControleInicio(String dados){
		this.nome = dados;
	}
	
	public static String createString(String nome){
		return "controle.inicio:"+nome;
	}
}
