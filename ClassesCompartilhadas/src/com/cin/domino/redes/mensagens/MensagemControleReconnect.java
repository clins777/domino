package com.cin.domino.redes.mensagens;

public class MensagemControleReconnect extends Mensagem{
	public String nome;
	public int idSala;
	
	public MensagemControleReconnect(String dados){
		String[] array = dados.split("[|]");
		this.nome = array[0];
		this.idSala = Integer.parseInt(array[1]);
	}
	public static String createString(String nome, int idSala){
		return "controle.reconnect:"+nome+"|"+idSala;
	}
}
