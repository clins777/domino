package com.cin.domino.redes.mensagens;


public class MensagemJogoJogada extends Mensagem{
	public int iDaPeca, jDaPeca;
	public boolean lado;
	/**
	 * formato = jogo.jogada:
	 * @param dados
	 */
	public MensagemJogoJogada(String dados)
	{
		//Decodificando a mensagem: "Peca|Lado"
		String parte[] = dados.split("[|]");
		//pecaParte = {i, j, x, y, p};
		String pecaParte[] = parte[0].split(";");
		iDaPeca = Integer.parseInt(pecaParte[0]);
		jDaPeca = Integer.parseInt(pecaParte[1]);
		lado = Boolean.parseBoolean(parte[1]);	
	}
	public static String createString(int i, int j, boolean lado)
	{
		return "jogo.jogada:"+i+";"+j+"|"+lado;
	}
}
