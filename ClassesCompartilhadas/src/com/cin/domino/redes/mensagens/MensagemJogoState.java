package com.cin.domino.redes.mensagens;

import java.util.ArrayList;
import java.util.Vector;

import com.cin.domino.game.logic.*;


public class MensagemJogoState extends Mensagem{
	public String[] jogadoresNome;
	public int[] jogadoresMaoQtd;
	public boolean[] jogadoresStatus;
	public String jogadorVezNome;
	public Tabuleiro tabuleiro;
	public Peca[] mao;
	public int[] pontucacao;
	public int idSala;
	public boolean suaVez;
	public MensagemJogoState(String dados){
		String[] array = dados.split("[|]");
		String[] listaJogadoresString = array[0].split(";");
		this.jogadoresMaoQtd = new int[3];
		this.jogadoresStatus = new boolean[3];
		this.jogadoresNome = new String[3];
		//|nome,qtdeMao,status;nome,qtdeMao,status;nome,qtdeMao,status|
		for (int i = 0; i < jogadoresNome.length; i++) {
			String[] dadosJogador = listaJogadoresString[i].split(",");
			jogadoresNome[i] = dadosJogador[0];
			jogadoresMaoQtd[i] = Integer.parseInt(dadosJogador[1]);
			jogadoresStatus[i] = Boolean.parseBoolean(dadosJogador[2]);
		}
		//|jogadorVezNome|
		this.jogadorVezNome = array[1];
		//peca inicial
		//|i,j|
		String [] pecaInicial = array[2].split(",");
		int i,j;
		i = Integer.parseInt(pecaInicial[0]);
		j = Integer.parseInt(pecaInicial[1]);
		tabuleiro = new Tabuleiro(new Peca(i, j));

		//pecas da mao
		//|i,j;i,j|'
		String[] listaPecasMao = array[3].split(";");
		this.mao = new Peca[listaPecasMao.length];
		if("".equals(listaPecasMao[0]) || listaPecasMao[0] == null){
			this.mao = new Peca[0];
		}
		for (int k = 0; k < mao.length; k++) {
			String[] pecaMao = listaPecasMao[k].split(",");
			i = Integer.parseInt(pecaMao[0]);
			j = Integer.parseInt(pecaMao[1]);
			this.mao[k] = new Peca(i, j);
		}

		String[] pontuacaoString  = array[4].split(";");
		this.pontucacao = new int[2];
		this.pontucacao[0] = Integer.parseInt(pontuacaoString[0]);
		this.pontucacao[1] = Integer.parseInt(pontuacaoString[1]);
		this.idSala = Integer.parseInt(array[5]);
		this.suaVez = Boolean.parseBoolean(array[6]);
		//tabuleiro
		if(array.length > 7)
		{
			String[] pecas0 = array[7].split(";");
			//percorre ao contrario o pecas0 
			for(int k = pecas0.length-1; k >= 0; k--){
				String[] peca = pecas0[k].split(",");
				i = Integer.parseInt(peca[0]);
				j = Integer.parseInt(peca[1]);
				this.tabuleiro.add(0, new Peca(i, j));
			}
			if(array.length > 8)
			{
				String[]pecas1 = array[8].split(";");
				for (int k = 0; k < pecas1.length; k++) {
					String[] peca = pecas1[k].split(",");
					i = Integer.parseInt(peca[0]);
					j = Integer.parseInt(peca[1]);
					this.tabuleiro.add(new Peca(i, j));
				}
			}
		}
	}
	public static String createString(String[] jogadoresNomes, int[] jogadoresMaoQtd, boolean[] jogadoresStatus,
			String jogadorVezNome,Tabuleiro tabuleiro, Peca[] mao, int[] pontucacao, int idSala, boolean suaVez){

		String out = "jogo.state:"+jogadoresNomes[0]+","+jogadoresMaoQtd[0]+","+jogadoresStatus[0];

		if(1<jogadoresNomes.length){
			for (int i = 1; i < jogadoresNomes.length; i++) {
				out = out+";"+jogadoresNomes[i]+","+jogadoresMaoQtd[i]+","+jogadoresStatus[i];
			}
		}

		out = out+"|"+jogadorVezNome;
		out = out+"|" + tabuleiro.getPecaInicial().getNumeros()[0] +","+ tabuleiro.getPecaInicial().getNumeros()[1];

		out = out + "|";
		for (Peca peca: mao) 
		{
			out = out +peca.getNumeros()[0]+","+peca.getNumeros()[1]+";";
		}
		if(mao.length > 0)
			out = out.substring(0,out.length()-1); //remove o �ltimo ';'

		out = out+"|";

		out = out+pontucacao[0]+";"+pontucacao[1];
		out = out+"|"+idSala;
		out = out + "|" + suaVez;


		out = out +"|";
		Vector<Peca> lista0 = tabuleiro.getListaLado0();
		for (Peca peca : lista0) {
			out = out+ peca.getNumeros()[0]+","+peca.getNumeros()[1]+";";
		}
		if(!lista0.isEmpty())
			out = out.substring(0,out.length()-1); //remove o �ltimo ';'

		out = out +"|";

		Vector<Peca> lista1 = tabuleiro.getListaLado1();
		for (Peca peca : lista1) {
			out = out+ peca.getNumeros()[0]+","+peca.getNumeros()[1]+";";
		}
		if(!lista1.isEmpty())
			out = out.substring(0,out.length()-1); //remove o �ltimo ';'
		return out;
	} 
}
