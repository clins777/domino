package com.cin.domino.redes.mensagens;

public class MensagemJogoEndPartida extends Mensagem {
	public int[] pontuacao;
	public boolean ganhou;
	public boolean empate;
	
	public MensagemJogoEndPartida(String dados){
		String[] array = dados.split("[|]");
		String[] pontos = array[0].split(";");
		pontuacao = new int[2];
		pontuacao[0] = Integer.parseInt(pontos[0]);
		pontuacao[1] = Integer.parseInt(pontos[1]);
		
		ganhou = Boolean.parseBoolean(array[1]);
		empate = Boolean.parseBoolean(array[2]);
	}
	public static String createString(int[] pontuacao, boolean ganhou, boolean empate){
		return "jogo.endPartida:"+pontuacao[0]+";"+pontuacao[1]+"|"+ganhou+"|"+empate;
	}
}
