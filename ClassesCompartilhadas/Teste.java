package com.cin.domino.game.logic;

import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.MessageListener;



public class Teste extends Thread implements MessageListener{

	private static int score[] = new int [2];
	private static Jogador j1;
	private static Jogador j2;
	private static Jogador j3;
	private static Jogador j4;
	private static Dupla dupla1;
	private static Dupla dupla2;
	private static Partida partida;
//	private static PlayerView PV; 
	private boolean recebeu;
	private Peca pecaJogada;
	private boolean lado;

//	public Teste(PlayerView PV){
//		this.PV = PV;
//	}

	//	public static void main(String[] args) {
	public void run(){
		lado = false;
		pecaJogada = null;
		recebeu = false;
		j1 = new Jogador("jogador 1",1);
		j2 = new Jogador("jogador 2",2);
		j3 = new Jogador("jogador 3",3);
		j4 = new Jogador("jogador 4",4);
		dupla1 = new Dupla(j1, j2);
		dupla2 = new Dupla(j3, j4);
		score[0]=0;
		score[1]=0;
		partida = new Partida(dupla1, dupla2);
		while(score[0]<7 && score[1]<7){
			partida.iniciarPartida(6);
			this.pecaJogada = partida.getTabuleiro().get(0);
			//jogo iniciado

			this.PV.Receptor(criarPacote());
			
			print(partida);	
			while(!partida.isFim() && partida.getContToque()<5){			
				if(recebeu)
				{
					
					print(partida);	
					partida.rodada(pecaJogada, lado);
					System.out.println("Passoou na l�gica!!");
					recebeu = false;
					this.PV.Receptor(criarPacote());
				}
			}
			if(partida.getContToque()==5)
				System.out.println("dupla 1: " + partida.getDuplas()[0].contarPecas() + "       dupla 2: " + partida.getDuplas()[1].contarPecas());
			Dupla duplaVencedora = partida.getDuplaVencedora();
			duplaVencedora.adicionarPontos(partida.pontosGanho(pecaJogada));
			score[0] = partida.getDuplas()[0].getPontos();
			score[1] = partida.getDuplas()[1].getPontos();
		}

	}



	public void Receptor(String M){
		//Separador =  "___" -> i,jPeca | X,Y,P | index | lado

		System.out.println(M);

		String partes[] = M.split("___");
		String ij[] = partes[0].split(",");
		String xyp[] = partes[1].split(",");
		int IJ[] = new int[2];
		float XYP[] = new float[3];
		IJ[0] = Integer.parseInt(ij[0]);
		IJ[1] = Integer.parseInt(ij[1]);
		XYP[0] = Float.parseFloat(xyp[0]);
		XYP[1] = Float.parseFloat(xyp[1]);
		XYP[2] = Float.parseFloat(xyp[2]);
		int index = Integer.parseInt(xyp[2]);
		Peca P =partida.getJogadorAtual().getMao().get(index);
		P.setX(XYP[0]);
		P.setY(XYP[1]);
		P.setP((int)XYP[2]);
		pecaJogada = P;
		if(Integer.parseInt(partes[3])==1)
			lado = true;
		else
			lado = false;



		recebeu = true;
		System.out.println(recebeu);

	}

	/**
	 *
	 * //CABECALHO -> PodeContinuar | FaltaJogador | Fim
		//Separador =  "___" -> Nome | ID | PontuacaoTotal | PontosJogadorAtual | PecasDaMaoSeparadasPor';' | pecasDoTabuleiroSeparadasPor';' | CABECALHO
	 */
	public String criarPacote(){

		//CABECALHO -> PodeContinuar | FaltaJogador | Fim
		//Separador =  "___" -> Nome | ID | PontuacaoTotal | PecasDaMaoSeparadasPor';' | pecasDoTabuleiroSeparadasPor';' | eixos/PosicaoPecasDoTabuleiroSeparadasPor',' | CABECALHO



		String message = "";
		message += partida.getJogadorAtual().getNome() + "___" + partida.getJogadorAtual().getID() + "___" + score[0] + "," + score[1] + "___";
		boolean aux=false;
		for(Peca peca : partida.getJogadorAtual().getMao()){
			if(aux) message += ";";
			else aux = true;
			message += peca.numeros[0] + "," + peca.numeros[1];
		}

		message += "___";

		aux=false;
		for(Peca peca : partida.getTabuleiro()){
			if(aux) message += ";";
			else aux = true;
			System.out.println("Tabuleiro peca > > >" +peca.numeros[0] + "," + peca.numeros[1]);
			message += peca.numeros[0] + "," + peca.numeros[1];
		}
		message += "___";
		aux=false;
		for(Peca peca : partida.getTabuleiro()){
			if(aux) message += ";";
			else aux = true;
			message += peca.getX() + "," + peca.getY() + "," + peca.getP();
		}


		message += "___" + "1;0";
		if(score[0]<7 && score[1]<7)
			message += ";" + "1";
		else
			message += ";" + "0";
		System.out.println("Pacote enviado do serv para cli: " + message);
		return message;		
	}

	static void print(Partida p){
		Tabuleiro t;
		t = p.getTabuleiro();
		int size = t.size()-1;
		int E0, E1,D0, D1;
		Peca ponta;
		ponta = t.get(0);
		if(ponta.getVizinhos()[0] == null){
			E0=ponta.getNumeros()[0];
			E1=ponta.getNumeros()[1];
		} else {
			E0=ponta.getNumeros()[1];
			E1=ponta.getNumeros()[0];
		}
		ponta = t.get(size);
		if(ponta.getVizinhos()[0] == null){
			D0=ponta.getNumeros()[1];
			D1=ponta.getNumeros()[0];
		} else {
			D0=ponta.getNumeros()[0];
			D1=ponta.getNumeros()[1];
		}
		System.out.println("Pontuacao   " + score[0] + " X " + score[1]);
		System.out.println();
		System.out.println();
		if(p.getTabuleiro().size()==1)
			System.out.println(E0 + "|" + E1);		
		else if(p.getTabuleiro().size()==2)
			System.out.println(E0 + "|" + E1 + "  -  " + D0 + "|" + D1);
		else
			System.out.println(E0 + "|" + E1 + "..." + D0 + "|" + D1);
		System.out.println();
		System.out.println();
		System.out.println("vez de:  " + p.getJogadorAtual().getNome());
		for (Peca peca : p.getJogadorAtual().getMao()) {			
			System.out.print(peca.getNumeros()[0] + "  ");
		}
		System.out.println();
		for (Peca peca : p.getJogadorAtual().getMao()) {			
			System.out.print(peca.getNumeros()[1] + "  ");
		}
		System.out.println();
	}



	@Override
	public void receiveMessage(String message, Connection connection) {
		Receptor(message);
	}



	@Override
	public void connectionFail(Connection connection) {
		System.out.println("erro de conexao");
		
	}
}
