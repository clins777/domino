package com.cin.domino;
import java.util.Vector;

@SuppressWarnings("serial")
public class TabuleiroGUI extends Vector<PecaGUI> {
	private PecaGUI pecaInicial;
	public TabuleiroGUI(PecaGUI pecaInicial)	{
		super();
		this.pecaInicial = pecaInicial;
		this.add(pecaInicial);
	}
	public TabuleiroGUI()
	{
		super();
	}
	public PecaGUI getPecaInicial() {
		return pecaInicial;
	}
	public String toString()
	{
		String resultado = new String();
		for (PecaGUI peca : this) {
			resultado += peca.toString() + " ";	
		}
		resultado+="\n";
		return resultado;
	}
}