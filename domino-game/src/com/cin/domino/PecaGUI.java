package com.cin.domino;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.cin.domino.game.logic.Peca;



public class PecaGUI extends Peca
{

	private boolean curva;
	private Sprite sprite;
	private Vector3 eixos[] = new Vector3[6];
	private int orientacao;
	private boolean fixa;
	private int index;
	private Vector2 posicaoChao;
	public void pegar()
	{
		this.sprite.setScale(1.3f);
		posicaoChao = new Vector2 (sprite.getX(), sprite.getY());
	}
	public void soltar()
	{
		//		setPosicao(posicaoChao.x, posicaoChao.y);
		sprite.setScale(1);
		sprite.setPosition(posicaoChao.x, posicaoChao.y);
	}
	public void setPosicao(Vector2 posicao)
	{
		sprite.setPosition(posicao.x-sprite.getWidth()/2, posicao.y-sprite.getHeight()/2);
	}
	public void setPosicao(float x, float y)
	{
		sprite.setPosition(x-sprite.getWidth()/2, y-sprite.getHeight()/2);
	}
	public void setScale(float s){
		this.sprite.setScale(s);
	}

	public void setFixa(boolean fixa) {
		this.fixa = fixa;
	}

	public boolean isFixa(){
		return this.fixa;
	}
	public Rectangle getBounds()
	{
		return sprite.getBoundingRectangle();
	}
	public PecaGUI(int i, int j, Texture textura, Rectangle retangulo)
	{
		super(i, j);
		index = -1;
		orientacao = 0;
		sprite = new Sprite(textura);
		sprite.setPosition(retangulo.x, retangulo.y);
		//		setPosicao(retangulo.x, retangulo.y);
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		fixa = false;
		curva = false;
		float h, w;
		h = sprite.getBoundingRectangle().height;
		w = sprite.getBoundingRectangle().width;
		eixos[0] = new Vector3(0 , -h/2 , 0 );
		eixos[1] = new Vector3(0 , h/2 , 0 );
		eixos[2] = new Vector3(-w/2 , 0 , 0 );
		eixos[3] = new Vector3(w/2 , 0 , 0 );
		eixos[4] = new Vector3(w/2, h/4, 0);
		eixos[5] = new Vector3(-w/2, -h/4, 0);
	}

	public int carroca() {
		/**Se a peca for carroca retorna o valor, senao for carroca retorna -1**/
		if(numeros[0]==numeros[1]) return numeros[0];
		else return -1;
	}

	public int getPosicao() {
		return orientacao;
	}

	// s� para teste
	public void setOrientacao(int P) {
		//EIXOS/////*//////*////////////////////////////////////////////////////////////////
		////////___*5*____*3*__________/////////////////////////////////////////////////////
		///////|           |           |////////////////////////////////////////////////////
		//////*|           |           |*///////////////////////////////////////////////////
		/////*0|    [0]    o    [1]    |1*//////////////////////////////////////////////////
		//////*|           |           |*///////////////////////////////////////////////////
		////>>>#___________|___________|////////////////////////////////////////////////////
		///////^//////////*2*////*4*/////////////////////////////////////////////////////////
		///////^///////////*//////*/////////////////////////////////////////////////////////
		float h, w;
		switch (P) {
		case 0:
			sprite.setRotation(0);
			h = sprite.getBoundingRectangle().height;
			w = sprite.getBoundingRectangle().width;
			eixos[0] = new Vector3(0 , -h/2 , 0 );
			eixos[1] = new Vector3(0 , h/2 , 0 );
			eixos[2] = new Vector3(-w/2 , 0 , 0 );
			eixos[3] = new Vector3(w/2 , 0 , 0 );
			eixos[4] = new Vector3(w/2, h/4, 0);
			eixos[5] = new Vector3(-w/2, -h/4, 0);
			break;
		case 1:
			sprite.setRotation(90);
			h = sprite.getBoundingRectangle().height;
			w = sprite.getBoundingRectangle().width;
			eixos[2] = new Vector3(0 , -h/2 , 0 );
			eixos[3] = new Vector3(0 , h/2 , 0 );
			eixos[1] = new Vector3(-w/2 , 0 , 0 );
			eixos[0] = new Vector3(w/2 , 0 , 0 );
			eixos[4] = new Vector3(w/4, -h/2, 0);
			eixos[5] = new Vector3(-w/4, h/2, 0);
			break;
		case 2:
			sprite.setRotation(180);
			h = sprite.getBoundingRectangle().height;
			w = sprite.getBoundingRectangle().width;
			eixos[1] = new Vector3(0 , -h/2 , 0 );
			eixos[0] = new Vector3(0 , h/2 , 0 );
			eixos[2] = new Vector3(-w/2 , 0 , 0 );
			eixos[3] = new Vector3(w/2 , 0 , 0 );
			eixos[4] = new Vector3(-w/2, -h/4, 0);
			eixos[5] = new Vector3(w/2, h/4, 0);
			break;
		default:
			sprite.setRotation(270);
			h = sprite.getBoundingRectangle().height;
			w = sprite.getBoundingRectangle().width;
			eixos[2] = new Vector3(0 , -h/2 , 0 );
			eixos[3] = new Vector3(0 , h/2 , 0 );
			eixos[0] = new Vector3(-w/2 , 0 , 0 );
			eixos[1] = new Vector3(w/2 , 0 , 0 );
			eixos[4] = new Vector3(-w/4, h/2, 0);
			eixos[5] = new Vector3(w/4, -h/2, 0);
			break;
		}
		orientacao = P;
	}

	public boolean merge (PecaGUI peca, boolean deveDarCurva) {

		curva = deveDarCurva;
		int P=orientacao;
		setOrientacao(P);
		peca.setOrientacao(peca.getPosicao());
		boolean retorno = false;
		int aux = peca.getPosicao();
		int encaixe = -1, eixo=-1;
		if(!deveDarCurva){
			if(numeros[0] == numeros[1]){
				// a que sera encaixada sera carroca
				if(peca.getNumeros()[0] == numeros[0] && peca.getVizinhos()[0]==null){
					switch(aux){
					case 0:

						P=3;
						break;
					case 1:
						P=0;
						break;
					case 2:
						P=1;
						break;
					default:
						P=2;
						break;
					}
					encaixe = 0;
					if(encaixe>=0)
						if(peca.getEixos()[encaixe].x > 0 || peca.getEixos()[encaixe].y > 0){
							eixo = 2;
						}else{
							eixo = 3;
						}
					vizinhos[0] = peca;
					retorno = true;
					peca.getVizinhos()[0] = this;
					System.out.println(">>>>>>>>>>>>>>>>>>0<<<<<<<<<<<<<<<<<<<<<");
				}else if(peca.getNumeros()[1] == numeros[0] && peca.getVizinhos()[1]==null){
					switch(aux){
					case 0:
						P=1;
						break;
					case 1:
						P=2;
						break;
					case 2:
						P=3;
						break;
					default:
						P=0;
						break;
					}
					encaixe = 1;
					if(encaixe>=0)
						if(peca.getEixos()[encaixe].x > 0 || peca.getEixos()[encaixe].y > 0){
							eixo = 2;
						}else{
							eixo = 3;
						}
					vizinhos[0] = peca;
					retorno = true;
					peca.getVizinhos()[1] = this;
					System.out.println(">>>>>>>>>>>>>>>>>>1<<<<<<<<<<<<<<<<<<<<<");
				}
			}else if(peca.getNumeros()[0] == peca.getNumeros()[1]){
				// a que esta encaixada e carroca
				if(peca.isCurva()){
					if(peca.getNumeros()[0] == numeros[0] && peca.getVizinhos()[0]==null){
						switch(aux){
						case 0:
							P=2;
							break;
						case 1:
							P=3;
							break;
						case 2:
							P=0;
							break;
						default:
							P=1;
							break;
						}
						eixo = 0;
						encaixe = 0;
						vizinhos[0] = peca;
						peca.getVizinhos()[0] = this;
						retorno = true;
					}else if(peca.getNumeros()[1] == numeros[1] && peca.getVizinhos()[1]==null){
						switch(aux){
						case 0:
							P=2;
							break;
						case 1:
							P=3;
							break;
						case 2:
							P=0;
							break;
						default:
							P=1;
							break;
						}
						eixo = 1;
						encaixe = 1;
						vizinhos[1] = peca;
						peca.getVizinhos()[1] = this;
						retorno = true;
					}else if(peca.getNumeros()[1] == numeros[0] && peca.getVizinhos()[1]==null){
						switch(aux){
						case 0:
							P=0;
							break;
						case 1:
							P=1;
							break;
						case 2:
							P=2;
							break;
						default:
							P=3;
							break;
						}
						eixo = 0;
						encaixe = 1;
						vizinhos[0] = peca;
						peca.getVizinhos()[1] = this;
						retorno = true;
					}else if(peca.getNumeros()[0] == numeros[1] && peca.getVizinhos()[0]==null){
						switch(aux){
						case 0:
							P=0;
							break;
						case 1:
							P=1;
							break;
						case 2:
							P=2;
							break;
						default:
							P=3;
							break;
						}
						eixo = 1;
						encaixe = 0;
						vizinhos[1] = peca;
						peca.getVizinhos()[0] = this;
						retorno = true;
					}
				}
				else
				{					


					if(peca.getVizinhos()[1]==null){
						if(peca.getNumeros()[0] == numeros[0]){
							switch(aux){
							case 0:
								P=3;
								break;
							case 1:
								P=0;
								break;
							case 2:
								P=1;
								break;
							default:
								P=2;
								break;
							}			
							eixo = 0;
							retorno = true;
							vizinhos[0] = peca;
						}else if(peca.getNumeros()[0] == numeros[1]){
							switch(aux){
							case 0:
								P=1;
								break;
							case 1:
								P=2;
								break;
							case 2:
								P=3;
								break;
							default:
								P=0;
								break;
							}			
							eixo = 1;
							retorno = true;
							vizinhos[1] = peca;
						}
						setOrientacao(P);
						if(eixo>=0)
							if(eixos[eixo].x > 0 || eixos[eixo].y > 0)
								encaixe = 2;
							else
								encaixe = 3;
						peca.getVizinhos()[1] = this;
					}else if(peca.getVizinhos()[0]==null){
						if(peca.getNumeros()[0] == numeros[0]){
							switch(aux){
							case 0:
								P=1;
								break;
							case 1:
								P=2;
								break;
							case 2:
								P=3;
								break;
							default:
								P=0;
								break;
							}		
							eixo = 0;	
							retorno = true;
							vizinhos[0] = peca;
						}else if(peca.getNumeros()[0] == numeros[1]){
							switch(aux){
							case 0:
								P=3;
								break;
							case 1:
								P=0;
								break;
							case 2:
								P=1;
								break;
							default:
								P=2;
								break;
							}
							eixo = 1;
							vizinhos[1] = peca;
							retorno = true;
						}
						encaixe = 2;
						peca.getVizinhos()[0] = this;
					}

				}
			}else if(peca.getNumeros()[0] == numeros[0] && peca.getVizinhos()[0]==null){
				switch(aux){
				case 0:
					P=2;
					break;
				case 1:
					P=3;
					break;
				case 2:
					P=0;
					break;
				default:
					P=1;
					break;
				}
				eixo = 0;
				encaixe = 0;
				vizinhos[0] = peca;
				peca.getVizinhos()[0] = this;
				retorno = true;
			}else if(peca.getNumeros()[1] == numeros[1] && peca.getVizinhos()[1]==null){
				switch(aux){
				case 0:
					P=2;
					break;
				case 1:
					P=3;
					break;
				case 2:
					P=0;
					break;
				default:
					P=1;
					break;
				}
				eixo = 1;
				encaixe = 1;
				vizinhos[1] = peca;
				peca.getVizinhos()[1] = this;
				retorno = true;
			}else if(peca.getNumeros()[1] == numeros[0] && peca.getVizinhos()[1]==null){
				switch(aux){
				case 0:
					P=0;
					break;
				case 1:
					P=1;
					break;
				case 2:
					P=2;
					break;
				default:
					P=3;
					break;
				}
				eixo = 0;
				encaixe = 1;
				vizinhos[0] = peca;
				peca.getVizinhos()[1] = this;
				retorno = true;
			}else if(peca.getNumeros()[0] == numeros[1] && peca.getVizinhos()[0]==null){
				switch(aux){
				case 0:
					P=0;
					break;
				case 1:
					P=1;
					break;
				case 2:
					P=2;
					break;
				default:
					P=3;
					break;
				}
				eixo = 1;
				encaixe = 0;
				vizinhos[1] = peca;
				peca.getVizinhos()[0] = this;
				retorno = true;
			}
		}
		else
		{
			////////////////////////////////////////////////////VVVVVVVVVVVVVVVVVVVVVVVVVV
			///CAIO
			if(peca.getNumeros()[0] == peca.getNumeros()[1]){
				// a que esta encaixada e carroca
				if(numeros[0] == peca.getNumeros()[0]){
					switch(aux){
					case 0:
						P=2;
						break;
					case 1:
						P=3;
						break;
					case 2:
						P=0;
						break;
					default:
						P=1;
						break;
					}
					eixo = 0;
					encaixe = 0;
					retorno = true;
					vizinhos[0] = peca;
				}
				else if(numeros[1] == peca.getNumeros()[0]){
					switch(aux){
					case 0:
						P=0;
						break;
					case 1:
						P=1;
						break;
					case 2:
						P=2;
						break;
					default:
						P=3;
						break;
					}
					eixo = 1;
					encaixe = 0;
					retorno = true;
					vizinhos[1] = peca;
				}
				peca.getVizinhos()[1] = this;
			}
			else if(peca.getNumeros()[0] == numeros[0] && peca.getVizinhos()[0] == null)
			{
				System.out.println("0x0");
				switch(aux){
				case 0:
					encaixe = 5;
					P=1;
					break;
				case 1:
					encaixe = 4;
					P=2;
					break;
				case 2:
					encaixe = 5;
					P=3;
					break;
				default:
					encaixe = 4;
					P=0;
					break;
				}
				eixo = 0;
				vizinhos[0] = peca;
				peca.getVizinhos()[0] = this;
				retorno = true;
			}
			else if(peca.getNumeros()[0] == numeros[1] && peca.getVizinhos()[0] == null)
			{
				System.out.println("0x1");
				switch(aux){
				case 0:
					P=3;
					encaixe = 5;
					break;
				case 1:
					encaixe = 4;
					P=0;
					break;
				case 2:
					encaixe = 5;
					P=1;
					break;
				default:
					encaixe = 4;
					P=2;
					break;
				}
				eixo = 1;
				vizinhos[1] = peca;
				peca.getVizinhos()[0] = this;
				retorno = true;
			}
			else if(peca.getNumeros()[1] == numeros[0] && peca.getVizinhos()[1] == null)
			{
				System.out.println("1x0");
				switch(aux){
				case 0:
					encaixe = 4;
					P=3;
					break;
				case 1:
					encaixe = 5;
					P=0;
					break;
				case 2:
					encaixe = 4;
					P=1;
					break;
				default:
					encaixe = 5;
					P=2;
					break;
				}
				eixo = 0;
				vizinhos[0] = peca;
				peca.getVizinhos()[1] = this;
				retorno = true;
			}
			else if(peca.getNumeros()[1] == numeros[1] && peca.getVizinhos()[1] == null)
			{
				System.out.println("1x1");
				switch(aux){
				case 0:
					encaixe = 4;
					P=1;
					break;
				case 1:
					encaixe = 5;
					P=2;
					break;
				case 2:
					encaixe = 4;
					P=3;
					break;
				default:
					encaixe = 5;
					P=0;
					break;
				}
				eixo = 1;
				vizinhos[1] = peca;
				peca.getVizinhos()[1] = this;
				retorno = true;
			}
		}
		/////////////////////////////////////////////////////////////////VVVVVVVVVVVVVVVVVV
		if(eixo != -1 && encaixe != -1){
			
			fixa = true;
			peca.setFixa(true);		
			System.out.println("P: " + P + "\nencaixe: " + encaixe + "\neixo: " + eixo);
			setOrientacao(P);
			encaixaPeca(peca, encaixe, eixo, deveDarCurva);
		}
		return retorno;
	}


	private void encaixaPeca(PecaGUI peca, int encaixe, int eixo, boolean curva){
		///////////////////*////////////////////////////////////////////////////////////////
		////////____5_____*3*__________/////////////////////////////////////////////////////
		///////|           |           |////////////////////////////////////////////////////
		//////*|           |           |*///////////////////////////////////////////////////
		/////*0|    [0]    o    [1]    |1*//////////////////////////////////////////////////
		//////*|           |           |*///////////////////////////////////////////////////
		////>>>#___________|___________|////////////////////////////////////////////////////
		///////^//////////*2*////4//////////////////////////////////////////////////////////
		///////^///////////*////////////////////////////////////////////////////////////////

		///////////////////*////////////////////////////////////////////////////////////////
		////////____4_____*2*__________/////////////////////////////////////////////////////
		///////|           |           |////////////////////////////////////////////////////
		//////*|           |           |*///////////////////////////////////////////////////
		/////*1|    [1]    o    [0]    |0*//////////////////////////////////////////////////
		//////*|           |           |*///////////////////////////////////////////////////
		////>>>#___________|___________|////////////////////////////////////////////////////
		///////^//////////*3*/////5/////////////////////////////////////////////////////////
		///////^///////////*////////////////////////////////////////////////////////////////
		// peca -> Peca que esta no tabuleiro 
		// eixo -> em que lado da peca jogada ira encaixar 
		// encaixe -> onde o eixo vai se fixar

		sprite.setPosition(peca.sprite.getX() - peca.getEixos()[encaixe].x + eixos[eixo].x , peca.sprite.getY() - peca.getEixos()[encaixe].y + eixos[eixo].y);
		//		setPosicao(peca.sprite.getX() - peca.getEixos()[encaixe].x + eixos[eixo].x , peca.sprite.getY() - peca.getEixos()[encaixe].y + eixos[eixo].y);
		System.out.println(">>>>>>>>>>>>>>>>>>ENCAIXE  ["+peca.getEixos()[encaixe].x+","+peca.getEixos()[encaixe].y+"]     PECA  ["+eixos[eixo].x+","+eixos[eixo].y+"]");
	}



	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public void setVizinhos(PecaGUI[] vizinhos) {
		this.vizinhos = vizinhos;
	}

	public int[] getNumeros() {
		return numeros;
	}

	public int getSoma() {
		return numeros[0]+numeros[1];
	}

	public String toString()
	{
		return numeros[0] + "-" +numeros[1];
	}

	public boolean overlaps(PecaGUI peca)
	{
		return sprite.getBoundingRectangle().overlaps(peca.sprite.getBoundingRectangle());
	}	

	public Sprite getSprite() {
		return sprite;
	}

	public void draw(SpriteBatch batch){
		sprite.draw(batch);
	}


	public Vector3[] getEixos() {
		return eixos;
	}

	public boolean isCurva()
	{
		return this.curva;
	}
	public void resetVizinhos() {
		vizinhos = new PecaGUI[2];
	}

}
