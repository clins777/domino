package com.cin.domino;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.Timer;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.MessageListener;
import com.cin.domino.redes.connection.TCPConnection;
import com.cin.domino.redes.connection.UDPConnection;
import com.cin.domino.redes.mensagens.Mensagem;
import com.cin.domino.redes.mensagens.MensagemControleQuit;
import com.cin.domino.redes.mensagens.MensagemControleReconnect;
import com.cin.domino.redes.mensagens.MensagemControleSalaOff;
import com.cin.domino.redes.mensagens.MensagemJogadorWaitBegin;
import com.cin.domino.redes.mensagens.MensagemJogoEndGame;
import com.cin.domino.redes.mensagens.MensagemJogoEndPartida;
import com.cin.domino.redes.mensagens.MensagemJogoState;
import com.cin.domino.telas.Tela;
import com.cin.domino.telas.TelaDisconnect;
import com.cin.domino.telas.TelaEspera;
import com.cin.domino.telas.TelaFim;
import com.cin.domino.telas.TelaGamePlay;
import com.cin.domino.telas.TelaInicial;

public class DominoGdx implements ApplicationListener, MessageListener, ActionListener
{
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private ShapeRenderer debugShapeRenderer;
	// Stuff for touch
	private Connection conexao;
	private Timer disconnectTimer;
	private static int reconectTime = 100000;
	private boolean reconecting;
	private static Sprite quit[];
	boolean isTouched;
	boolean wasTouched;
	public Vector2 touch;
	private void updateTouchInput()
	{
		wasTouched = isTouched;
		isTouched = Gdx.input.isTouched();
		touch = DominoGdx.getToqueTela();
	}
	public boolean isTapped()
	{
		return isTouched && !wasTouched;
	}
	public boolean toqueNaPeca(PecaGUI peca)
	{
		return peca.getBounds().contains(touch.x, touch.y);
	}

	public Connection getConexao() {
		return conexao;
	}
	public static Vector2 getToqueTela()
	{
		return new Vector2(Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY());
	}
	private static int porta = 1001;
	//Telas
	private Tela telaAtual;
	private TelaInicial telaInicial;
	private TelaEspera telaEspera;
	private TelaGamePlay telaGamePlay;
	private TelaFim telaFim;
	private TelaDisconnect telaDisconnect;
	public String ipServidor;
	public String nome;
	public int idSala;
	@Override
	public void create()
	{
		Texture.setEnforcePotImages(false);
		disconnectTimer = new Timer(reconectTime, this);
		porta = new Random().nextInt(500)+8000;
		Gdx.graphics.setTitle("Domino");
		//carregando assets das telas
		TelaInicial.load();
		TelaEspera.load();
		TelaGamePlay.load();
		TelaDisconnect.load();
		TelaFim.load();
		telaAtual = telaInicial;
		//carregando config.txt

		quit = new Sprite[2]; 
		quit[0] = new Sprite(new Texture(Gdx.files.internal("texturas/quitOff.png")));
		quit[0].setPosition(Gdx.graphics.getWidth()-quit[0].getWidth(), Gdx.graphics.getHeight()-quit[0].getHeight());
		quit[1] = new Sprite(new Texture(Gdx.files.internal("texturas/quitOn.png")));
		quit[1].setPosition(Gdx.graphics.getWidth()-quit[1].getWidth(), Gdx.graphics.getHeight()-quit[1].getHeight());


		camera = new OrthographicCamera();

		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.position.set(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, 0);
		batch = new SpriteBatch();
		debugShapeRenderer = new ShapeRenderer();
		FileHandle txt = Gdx.files.external("data/config.txt");
		System.out.println("teste");
		System.out.println(Gdx.files.getExternalStoragePath());
		System.out.println(txt.exists());
		if(txt.exists())
		{
			Scanner in = new Scanner(txt.readString());
			String nome = in.next();
			String iPServ = in.next();
			int IDSala = in.nextInt();
			System.out.println(nome + "  -  " + iPServ + "  -  " + IDSala);
			telaInicial = new TelaInicial(this, nome, iPServ, IDSala);
			in.close();
		}
		else
		{
			telaInicial = new TelaInicial(this);
		}
		telaAtual = telaInicial;
		telaInicial.start();
	}

	public void tryReconnect(int num){

		FileHandle txt = Gdx.files.external("data/config.txt");
		System.out.println("tentando reconectar.");
		System.out.println(Gdx.files.getExternalStoragePath());
		System.out.println(txt.exists());
		if(txt.exists())
		{
			for(int i=0; i<num && conexao.isOnline(); i++){
				Scanner in = new Scanner(txt.readString());
				String nome = in.next();
				String iPServ = in.next();
				int IDSala = in.nextInt();
				System.out.println(nome + "  -  " + iPServ + "  -  " + IDSala);
				telaInicial = new TelaInicial(this, nome, iPServ, IDSala);
				in.close();
			}
		}else
		{
			telaInicial = new TelaInicial(this);
		}
		telaAtual = telaInicial;
		telaInicial.start();
	}

	public void reset(){
//		FileHandle txt = Gdx.files.external("data/config.txt");
//		if(txt.exists()) txt.delete();
		///////////txt.delete();
		//		conexao.fechar();
		telaInicial = new TelaInicial(this);
		telaAtual = telaInicial;
		telaInicial.start();
	}
	public void close()
	{
		FileHandle txt = Gdx.files.external("data/config.txt");
		if(txt.exists()) txt.delete();
		Gdx.app.exit();
		
	}
	@Override
	public void dispose()
	{
		batch.dispose();
	}
	public boolean toqueNoQuit(){
		return isTapped() && quit[0].getBoundingRectangle().contains(touch.x, touch.y);
	}
	@Override
	public void render()
	{		
		updateTouchInput();
		if(toqueNoQuit()){
			this.getConexao().sendMessage(MensagemControleQuit.createString());
			this.close();
		}
		Gdx.gl.glClearColor(0, 0, 0.5f, 0.5f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		telaAtual.render(batch);	
		if(toqueNoQuit() && isTapped()){
			quit[0].draw(batch);
		}else{
			quit[1].draw(batch);
		}
		batch.end();
	
		//		debugShapeRenderer.setProjectionMatrix(camera.combined);
		//		debugShapeRenderer.begin(ShapeType.Rectangle);
		//		Rectangle r = TelaGamePlay.getGameAreaBounds();
		//		debugShapeRenderer.rect(r.x, r.y, r.getWidth(), r.getHeight());
		//		debugShapeRenderer.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	public void receiveMessage(String message, Connection connection) {
		Mensagem mensagem = Mensagem.decodificaMensagem(message);
		if(mensagem instanceof MensagemJogadorWaitBegin)
		{
			if(this.telaAtual == this.telaInicial ||this.telaAtual == this.telaEspera)
			{
				MensagemJogadorWaitBegin waitBegin = (MensagemJogadorWaitBegin) mensagem;
				if(this.telaAtual == this.telaInicial)
					telaEspera = new TelaEspera(this);
				telaEspera.setNumDeJogaroresFaltando(waitBegin.playersRemain);
				this.telaAtual = telaEspera;
			}else
			{
				System.out.println("tentou ir para a tela de espera de outra tela");
			}
		}
		if(mensagem instanceof MensagemJogoState)
		{
			reconecting = false;
			FileHandle arquivo = Gdx.files.external("data/config.txt");
			this.nome = telaInicial.getNome();
			this.ipServidor = conexao.getIP();
			this.idSala = ((MensagemJogoState)mensagem).idSala;
			String data = this.telaInicial.getNome() + " " + conexao.getIP() + " " + ((MensagemJogoState)mensagem).idSala;
			arquivo.writeString(data, false);
			if(this.telaAtual != telaGamePlay)
			{
				this.telaGamePlay = new TelaGamePlay(this, telaInicial.getNome());
				this.telaAtual = telaGamePlay;
			}
			this.telaGamePlay.update((MensagemJogoState)mensagem);
		}
		if(mensagem instanceof MensagemControleSalaOff)
		{
			if(this.telaAtual == telaInicial)
			{
				this.telaInicial.salaOff();
			}
		}
		if(mensagem instanceof MensagemJogoEndPartida)
		{
			if(this.telaAtual == telaGamePlay)
			{	
				if(((MensagemJogoEndPartida) mensagem).ganhou){
					this.telaGamePlay.fimDePartida(true);
				}else if(((MensagemJogoEndPartida) mensagem).empate){
					this.telaGamePlay.empate();
				}else{
					this.telaGamePlay.fimDePartida(false);
				}
			}
		}
		if(mensagem instanceof MensagemJogoEndGame)
		{
			if(this.telaAtual == telaGamePlay)
			{			
				this.telaFim = new TelaFim(this);
				this.telaFim.update((MensagemJogoEndGame)mensagem);
				FileHandle txt = Gdx.files.external("data/config.txt");
				if(txt.exists()) txt.delete();
				this.telaAtual = this.telaFim;
			}
		}
	}
	public void connectionFail(Connection connection) {
		System.out.println("erro de conexao");
		if(telaAtual == telaGamePlay){
//			telaAtual = telaInicial;
			disconnectTimer.start();
//			reconecting = true;
			telaDisconnect = new TelaDisconnect(this);
			telaAtual = telaDisconnect;
			
//			this.tryReconnect(10000);
		}else{
			this.reset();
		}
	}

	public void StartConnection(String IP) {
		if(telaInicial.isTCP())
			this.conexao = new TCPConnection(IP, "Servidor", porta, this);
		else
		{
			Random rand = new Random();
			this.conexao = new UDPConnection(IP, "Servidor", 8000+rand.nextInt(500), this);
		}
		conexao.start();
	}

	@Override
	public void connectionOn(Connection connection) {
		if(reconecting)
		{
			conexao.sendMessage(MensagemControleReconnect.createString(nome, idSala));	
		} else
		if(telaAtual == telaInicial && telaInicial != null)
		{
			telaInicial.connectionSucess();
		}

	}
	public TelaInicial getTelaInicial() {
		return telaInicial;
	}
	public TelaEspera getTelaEspera() {
		return telaEspera;
	}
	public TelaGamePlay getTelaGamePlay() {
		return telaGamePlay;
	}
	public TelaFim getTelaFim() {
		return telaFim;
	}
	public void setTelaAtual(Tela telaAtual) {
		this.telaAtual = telaAtual;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
			Gdx.app.exit();
	}


}
