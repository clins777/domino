package com.cin.domino.telas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cin.domino.DominoGdx;
import com.cin.domino.redes.mensagens.MensagemControleInicio;
import com.cin.domino.redes.mensagens.MensagemControleReconnect;

public class TelaInicial extends Tela implements TextInputListener{
	enum Estado
	{
//		tentandoReconectar, preEntradaIP, falhaConexaoIP, esperandoEntradaIP, esperandoConexao, preEntradaNome, esperandoEntradaNome, ocioso
		tentaReconectar, entradaIP, entradaNome, nomeInvalido, escolhaProtocolo
	}
	private static Texture backGround;
	private String nome;
	private String ipServidor;
	private int idSala;
	private Estado estado;
	private boolean TCP;
	
	public TelaInicial(DominoGdx jogo) {
		super(jogo);
	}
	public TelaInicial(DominoGdx jogo, String nome, String ipServidor, int idSala)
	{
		
		super(jogo);
		this.nome = nome;
		this.ipServidor = ipServidor;
		this.idSala = idSala;
	}
	public void start()
	{
		setEstado(Estado.escolhaProtocolo);
		Gdx.input.getTextInput(this, "digite 0 para tcp, 1 para udp", "0");
	}
	public static void load()
	{
		backGround= new Texture("texturas/Capa.png");
	}
	public void render(SpriteBatch batch) {
		batch.draw(backGround, 0, 0);
	}
	public void input(String text) {
		if(estado == Estado.escolhaProtocolo)
		{
			if("0".equals(text))
			{
				TCP = true;
				if(nome == null)
				{
					setEstado(Estado.entradaIP);	
				}
				else
				{
					setEstado(Estado.tentaReconectar);
				}
			}
			else if("1".equals(text))
			{
				TCP = false;
				if(nome == null)
				{
					setEstado(Estado.entradaIP);	
				}
				else
				{
					setEstado(Estado.tentaReconectar);
				}
			}
			else
			{
				Gdx.input.getTextInput(this, "opcao invalida, escolha 0 para tcp ou 1 para udp",  "0"); 
			}
				
			
		}else if(estado == Estado.entradaIP)
		{
			this.ipServidor = text;
			this.jogo.StartConnection(text);
		}
		else
		if(estado == Estado.entradaNome)
		{
			boolean alfaNumerico = true;
			int size = text.length();
			for(int i=0; i<size; i++){
				char c = text.charAt(i);
				if(!Character.isLetterOrDigit(c))
					alfaNumerico = false;
			}
			if(size>0 && size<12 && alfaNumerico){
				this.nome = text;			
				Gdx.graphics.setTitle("Domino   "+nome);
				jogo.getConexao().sendMessage(MensagemControleInicio.createString(this.nome));
			}else{
				Gdx.input.setOnscreenKeyboardVisible(true);
				Gdx.input.getTextInput(this, "Nome invalido, tente usar um nome com o tamanho menor que 12 caracteres alfanumericos.", "");
			}
		}
		Gdx.input.setOnscreenKeyboardVisible(false);
	}
	public void canceled() {
		Gdx.app.exit();
	}

	public void nomeOcupado()
	{
		Gdx.input.setOnscreenKeyboardVisible(true);
		Gdx.input.getTextInput(this, "Nome ja ocupado", "");
	}
	public void connectionFail() {
		this.setEstado(Estado.entradaIP);
	}
	public void salaOff()
	{
		this.setEstado(Estado.entradaIP);
	}
	public void connectionSucess()
	{
		if(estado == Estado.tentaReconectar)
		{
			jogo.getConexao().sendMessage(MensagemControleReconnect.createString(this.nome, this.idSala));
		}
		else if (estado == Estado.entradaIP)
		{
			Gdx.input.setOnscreenKeyboardVisible(true);
			Gdx.input.getTextInput(this, "Digite o seu nome", "");
			this.setEstado(Estado.entradaNome);
		}
	}
	private void setEstado(Estado estado)
	{
		switch (estado) {
		case entradaIP:			
			Gdx.input.setOnscreenKeyboardVisible(true);
			if(this.estado == Estado.entradaIP)
				Gdx.input.getTextInput(this, "Nao conseguiu se conectar ao servidor, digite o IP novamente", "");
			else
				Gdx.input.getTextInput(this, "Digite o IP do Servidor", "");
			break;
		case tentaReconectar:
			jogo.StartConnection(ipServidor);
			break;
		default:
			break;
		}
		this.estado = estado;
	}
	public String getNome()
	{
		return this.nome;
	}
	public boolean isTCP() {
		return TCP;
	}
}
