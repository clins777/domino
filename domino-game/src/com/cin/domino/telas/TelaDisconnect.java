package com.cin.domino.telas;

import sun.java2d.windows.GDIBlitLoops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.cin.domino.DominoGdx;

public class TelaDisconnect extends Tela {
	
	private static Texture backGround;
	private static BitmapFont fonte;
	
	public TelaDisconnect(DominoGdx jogo) {
		super(jogo);
	}
	

	public static Texture getBackGround() {
		return backGround;
	}


	public static void load(){
		backGround= new Texture("texturas/fundoDisconnect.png");
		fonte = new BitmapFont(Gdx.files.internal("data/fonte.fnt"), false);
		fonte.setColor(Color.WHITE);
//		fonte = new BitmapFont();
//		fonte.scale(3);
	}

	public void render(SpriteBatch batch){
		String message = "Voce foi desconectado, por favor reinicie o programa para retornar ao jogo";
		TextBounds textBound = fonte.getMultiLineBounds(message);
		batch.draw(backGround, 0, 0);
		fonte.draw(batch,  message, (Gdx.graphics.getWidth()/2)-(textBound.width/2), (Gdx.graphics.getHeight()/2)-(textBound.height/2));
	}

}
