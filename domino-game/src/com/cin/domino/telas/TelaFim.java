package com.cin.domino.telas;

import sun.java2d.windows.GDIBlitLoops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.cin.domino.DominoGdx;
import com.cin.domino.redes.mensagens.MensagemJogoEndGame;

public class TelaFim extends Tela {
	
	private static Texture backGround;
	private static BitmapFont fonte;
	private String message;
	
	public TelaFim(DominoGdx jogo) {
		super(jogo);
		message = "";
	}


	public static Texture getBackGround() {
		return backGround;
	}


	public static void load(){
		backGround= new Texture("texturas/CapaEspera.png");
		fonte = new BitmapFont(Gdx.files.internal("data/fonte.fnt"), false);
		fonte.setColor(Color.BLACK);
		fonte.setScale(1.5f);
//		fonte = new BitmapFont();
//		fonte.scale(3);
	}

	public void update(MensagemJogoEndGame m){
		if(m.pontuacao[0]>m.pontuacao[1]){
			message = "VOCE VENCEU!\n   " +"7 X " + m.pontuacao[1];
		}else{
			message = "VOCE PERDEU!\n   " + m.pontuacao[1] + " X 7";
		}
	}
	
	public void render(SpriteBatch batch){
		TextBounds textBound = fonte.getMultiLineBounds(message);
		batch.draw(backGround, 0, 0);
		fonte.drawMultiLine(batch,  message, (Gdx.graphics.getWidth()/2)-(textBound.width/2), (Gdx.graphics.getHeight()/2)-(textBound.height/2));
		for(float cont=0; cont<5; cont+=Gdx.graphics.getDeltaTime());
		jogo.setTelaAtual(jogo.getTelaInicial());
	}

}
