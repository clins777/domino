package com.cin.domino.telas;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cin.domino.DominoGdx;

public abstract class Tela {
	protected DominoGdx jogo;
	
	public Tela(DominoGdx jogo){
		this.jogo = jogo;
	}
	public abstract void render(SpriteBatch batch);
}
