package com.cin.domino.telas;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cin.domino.DominoGdx;
import com.cin.domino.PecaGUI;
import com.cin.domino.TabuleiroGUI;
import com.cin.domino.game.logic.Peca;
import com.cin.domino.redes.mensagens.MensagemControleQuit;
import com.cin.domino.redes.mensagens.MensagemJogoJogada;
import com.cin.domino.redes.mensagens.MensagemJogoState;

enum Estado
{
	MINHA_VEZ, SEGURANDO_PECA, VEZ_DE_OUTRO
}
public class TelaGamePlay extends Tela implements TextInputListener{
	private static PecaGUI todasPecas[][];
	private TabuleiroGUI tabuleiro;
	private static Texture backGround;
	private static Texture escurece;
	private static BitmapFont fonte;
	private String nomes[];
	private String jogadorDaVez;
	private int numPecasJogadores[];
	private boolean statusJogadores[];
	private PecaGUI mao[];
	private PecaGUI pecaSelecionada;
	private int score[];
	private String nome;
	private Estado estado;
	private static Sprite blocoMao;
	private static Sprite blocoOutrosJogadores[][];
	private static Sprite placarMeu;
	private static Sprite placarAdversario;
	private static Sprite PontuacaoDuplas[][];
	private static Sprite statusOutrosJogadores[][];
	private String Mensagem;
	private boolean msgJogadorDaVez;
	private float cronometroMsg;
	private static Vector2 getTamanhoPeca()
	{
		return new Vector2(64, 128);
	}

	public void zera(){
		cronometroMsg = 0;
		msgJogadorDaVez = false;
		tabuleiro = new TabuleiroGUI();
		jogadorDaVez = "";
		nomes= new String[3];
		nomes[0] = "";
		nomes[1] = "";
		nomes[2] = "";
		numPecasJogadores = new int[3];
		statusJogadores = new boolean[3];
		jogadorDaVez = "NULL";
		Mensagem = "";
		tabuleiro = new TabuleiroGUI();
		mao = new PecaGUI[1];
		score = new int[2];
		estado = Estado.VEZ_DE_OUTRO;
		index0Aux = 0;
		index1Aux = 0;
		counter0 = 0;
		counter1 = 0;
	}

	private int counter0 = 0;
	private int counter1 = 0;
	private static int curvas0[];
	private static int curvas1[];
	private int index0Aux;
	private int index1Aux;

	/**
	 * 
	 * @param lado direita = true, esquerda = false
	 * @return
	 */
	private boolean podeGirar(boolean lado, PecaGUI P)
	{
		int aux=0;
		if(lado)
		{

			System.out.println(index1Aux);
			aux = counter1;
			if(counter1==0 && index1Aux!=0)
				counter1+=2;
			else
				if(P.getNumeros()[0] == P.getNumeros()[1])
					counter1++;
				else
					counter1+=2;
				
			System.out.println(aux);
			if(counter1>curvas1[index1Aux]){
				counter1=0;
				if(index1Aux==0){
					curvas1[2] = aux+6;
				}if(index1Aux==1){
					curvas1[3] = aux-3;
				}
				if(index1Aux<6)
					index1Aux++;
				System.out.println(index1Aux);
				return true;
			}
		}
		else
		{
			System.out.println(index0Aux);
			aux = counter0;
			if(counter0!=0 && index0Aux!=0)
				counter0+=2;
			else
				if(P.getNumeros()[0] == P.getNumeros()[1])
					counter0++;
				else
					counter0+=2;
			System.out.println(aux);
			if(counter0>curvas0[index0Aux]){
				counter0=0;
				if(index0Aux==0){
					curvas0[2] = aux+6;
				}if(index0Aux==1){
					curvas0[3] = aux-3;
				}
				if(index0Aux<6)
					index0Aux++;
				System.out.println(index0Aux);
				return true;
			}
		}
		return false;
	}

	public static Rectangle getGameAreaBounds()
	{
		Vector2 peca = getTamanhoPeca();
		//		return new Rectangle(0, 2f*peca.x, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		return new Rectangle(2.2f*peca.x, 1.9f*peca.x, Gdx.graphics.getWidth()- 4.4f*peca.x, Gdx.graphics.getHeight() - 3.8f*peca.x);
	}
	public static Rectangle getSafetyBounds()
	{
		Vector2 peca = getTamanhoPeca();
		float factor = 2f;
		return new Rectangle(factor*peca.x, factor*peca.x, Gdx.graphics.getWidth()- factor*peca.x, Gdx.graphics.getHeight() - factor*peca.x);
	}
	public TelaGamePlay(DominoGdx jogo, String nome) {
		super(jogo);
		cronometroMsg = 0;
		msgJogadorDaVez = false;
		this.nome = nome;
		tabuleiro = new TabuleiroGUI();
		jogadorDaVez = "";
		nomes= new String[3];
		nomes[0] = "";
		nomes[1] = "";
		nomes[2] = "";
		numPecasJogadores = new int[3];
		statusJogadores = new boolean[3];
		jogadorDaVez = "NULL";
		Mensagem = "";
		tabuleiro = new TabuleiroGUI();
		mao = new PecaGUI[1];
		score = new int[2];
		estado = Estado.VEZ_DE_OUTRO;
		curvas0 = new int[6];
		curvas1 = new int[6];
		curvas0[0] = 6;
		curvas0[1] = 7;
		curvas0[5] = 4;
		curvas1[0] = 6;
		curvas1[1] = 7;
		curvas1[5] = 4;
		index0Aux = 0;
		index1Aux = 0;
		counter0 = 0;
		counter1 = 0;

	}


	public static Texture getBackGround() {
		return backGround;
	}

	public static Vector2 posicoesOutrosJogadores(int i, Sprite s){
		switch (i){
		case 0:
			return new Vector2(Gdx.graphics.getWidth()-s.getWidth(), (Gdx.graphics.getHeight()/2)-s.getHeight()/2);
		case 1:
			return new Vector2((Gdx.graphics.getWidth()/2)-s.getWidth()/2, Gdx.graphics.getHeight()-s.getHeight());
		case 2:
			return new Vector2(0, (Gdx.graphics.getHeight()/2)-s.getHeight()/2);
		default:
			return new Vector2(0, 0);
		}
	}

	private Vector2 posicaoNome(int i){
		Vector2 v = new Vector2(fonte.getBounds(nomes[i]).width,fonte.getBounds(nomes[i]).height);
		switch (i){
		case 0:
			return new Vector2((Gdx.graphics.getWidth()-(blocoOutrosJogadores[0][0].getWidth()/2)-v.x/2), ((Gdx.graphics.getHeight()/2)+blocoOutrosJogadores[0][0].getHeight()/2)-v.y);
		case 1:
			return new Vector2((Gdx.graphics.getWidth()/2)-v.x/2, Gdx.graphics.getHeight()-v.y);
		case 2:
			return new Vector2((blocoOutrosJogadores[0][0].getWidth()/2)-v.x/2, ((Gdx.graphics.getHeight()/2)+blocoOutrosJogadores[0][0].getHeight()/2)-v.y);
		default:
			return new Vector2(0, 0);
		}
	}

	public static Vector2 posicoesPlacares(int i, Sprite s){
		switch (i){
		case 0:
			return new Vector2(Gdx.graphics.getWidth()-s.getWidth(), 0);
		case 1:
			return new Vector2(0, Gdx.graphics.getHeight()-s.getHeight());
		default:
			return new Vector2(0, 0);
		}
	}

	public Vector2 posPecaCentral(){
		return new Vector2(getGameAreaBounds().getX()+getGameAreaBounds().getWidth()/2,getGameAreaBounds().getY()+getGameAreaBounds().getHeight()/2);
	}

	public static void load(){		
		escurece= new Texture(Gdx.files.internal("texturas/Escurece.png"));
		backGround= new Texture(Gdx.files.internal("texturas/fundoJogo.png"));
		blocoMao = new Sprite(new Texture(Gdx.files.internal("texturas/bloco.png")));
		placarMeu = new Sprite(new Texture(Gdx.files.internal("texturas/MeuPlacar.png")));
		placarAdversario = new Sprite(new Texture(Gdx.files.internal("texturas/OponentePlacar.png")));
		blocoOutrosJogadores = new Sprite[3][7]; 
		PontuacaoDuplas = new Sprite[2][7];
		statusOutrosJogadores = new Sprite[3][2];
		for(int i=0; i<2; i++){
			for(int k=0; k<7;k++){
				PontuacaoDuplas[i][k] = new Sprite(new Texture(Gdx.files.internal("texturas/Placar"+k+".png")));
				PontuacaoDuplas[i][k].setPosition(posicoesPlacares(i, PontuacaoDuplas[i][k]).x, posicoesPlacares(i, PontuacaoDuplas[i][k]).y);
			}
		}
		for(int i = 0; i<3; i++){
			for(int k = 0; k<7 ; k++){
				blocoOutrosJogadores[i][k] = new Sprite(new Texture(Gdx.files.internal("texturas/blocoJogador"+ k +".png")));
			}
			statusOutrosJogadores[i][0] = new Sprite(new Texture(Gdx.files.internal("texturas/ON.png")));
			statusOutrosJogadores[i][1] = new Sprite(new Texture(Gdx.files.internal("texturas/OFF.png")));
		}

		fonte = new BitmapFont(Gdx.files.internal("data/fonte.fnt"), false);
		fonte.setColor(Color.WHITE);
		todasPecas = new PecaGUI[7][7];
		for(int i=0; i<7; i++)
		{
			for(int j=0; j<=i;j++)
			{
				FileHandle handle = Gdx.files.internal("stones/" + i + "-" + j + ".png");
				Texture textura = new Texture(handle);
				Rectangle retangulo = new Rectangle(-666, -666, 64, 128);
				todasPecas[i][j] = new PecaGUI(i, j, textura, retangulo);
			}
		}
		blocoMao.setPosition(((Gdx.graphics.getWidth()/2)-blocoMao.getWidth()/2)-6, 0);
		placarMeu.setPosition(posicoesPlacares(0, placarMeu).x, posicoesPlacares(0, placarMeu).y);
		placarAdversario.setPosition(posicoesPlacares(1, placarAdversario).x, posicoesPlacares(1, placarAdversario).y);

	}

	public synchronized void update(MensagemJogoState jogoJogada){
		index0Aux = 0;
		index1Aux = 0;
		counter0 = 0;
		counter1 = 0;
		pecaSelecionada = null;
		msgJogadorDaVez = true;
		cronometroMsg = 0;
		Mensagem = "";
		System.out.println("Oi, eu sou o " + nome);
		jogadorDaVez = jogoJogada.jogadorVezNome;
		counter0 = 0;
		counter1 = 0;
		
		for(int i=0; i<7; i++)
		{
			for(int j=0; j<=i;j++)
			{
				todasPecas[i][j].setFixa(true);
				todasPecas[i][j].setPosicao(-666, -666);
				todasPecas[i][j].setOrientacao(0);
				todasPecas[i][j].resetVizinhos();
				todasPecas[i][j].getSprite().setColor(Color.WHITE);
				todasPecas[i][j].getSprite().setScale(1);
			}
		}
		PecaGUI pecaInicial = todasPecas[jogoJogada.tabuleiro.getPecaInicial().getNumeros()[0]][jogoJogada.tabuleiro.getPecaInicial().getNumeros()[1]];
		pecaInicial.setScale((float)0.4);
		pecaInicial.setOrientacao(1);
		pecaInicial.getSprite().setPosition(posPecaCentral().x-pecaInicial.getSprite().getWidth()/2, posPecaCentral().y-pecaInicial.getSprite().getHeight()/2);
		TabuleiroGUI tabuleiroAux = new TabuleiroGUI(pecaInicial);
		PecaGUI PAnterior = pecaInicial;
		for(int i=jogoJogada.tabuleiro.getListaLado0().size()-1; i>-1; i--)
		{
			Peca peca = jogoJogada.tabuleiro.getListaLado0().get(i);
			PecaGUI P = todasPecas[peca.getNumeros()[0]][peca.getNumeros()[1]];
			P.setScale((float)0.4);
			P.merge(PAnterior, podeGirar(false,P));
			P.setFixa(true);
			PAnterior = P;
			tabuleiroAux.add(0,P);
		}
		PAnterior = pecaInicial;
		for(int i=0; i<jogoJogada.tabuleiro.getListaLado1().size(); i++)
		{
			Peca peca = jogoJogada.tabuleiro.getListaLado1().get(i);
			PecaGUI P = todasPecas[peca.getNumeros()[0]][peca.getNumeros()[1]];
			P.setScale((float)0.4);
			P.merge(PAnterior, podeGirar(true,P));
			P.setFixa(true);
			PAnterior = P;
			tabuleiroAux.add(tabuleiroAux.size(),P);
		}
		tabuleiro = tabuleiroAux;
		nomes = jogoJogada.jogadoresNome;
		numPecasJogadores = jogoJogada.jogadoresMaoQtd;
		statusJogadores = jogoJogada.jogadoresStatus;
		PecaGUI maoAux[] = new PecaGUI[jogoJogada.mao.length];
		float meio = Gdx.graphics.getWidth()/2;
		float espacoOcupadoPleaPeca = 70;
		float posAux = -((jogoJogada.mao.length*espacoOcupadoPleaPeca)/2);
		for(int i=0; i<jogoJogada.mao.length; i++){	
			maoAux[i] = todasPecas[jogoJogada.mao[i].getNumeros()[0]][jogoJogada.mao[i].getNumeros()[1]];
			maoAux[i].getSprite().setPosition(meio+posAux,20);
			maoAux[i].setOrientacao(0);
			posAux += espacoOcupadoPleaPeca;
		}
		mao = maoAux;
		score = jogoJogada.pontucacao;
		System.out.println("sua Vez: " + jogoJogada.suaVez);
		if(jogoJogada.suaVez)
			this.estado = Estado.MINHA_VEZ;
		else
			this.estado = Estado.VEZ_DE_OUTRO;
		Mensagem = "Vez de "+jogadorDaVez;
	}

	public void render(SpriteBatch batch){
		batch.draw(backGround,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		for(PecaGUI peca : tabuleiro){
			peca.getSprite().draw(batch);
			peca.getSprite().setScale((float)0.4);
		}
		for(int i=0; i<3; i++){
			blocoOutrosJogadores[i][numPecasJogadores[i]].setPosition(posicoesOutrosJogadores(i, blocoOutrosJogadores[i][numPecasJogadores[i]]).x,posicoesOutrosJogadores(i, blocoOutrosJogadores[i][numPecasJogadores[i]]).y);
			int statusAux;
			if(statusJogadores[i]){
				statusAux = 0;
			}else{
				statusAux = 1;
			}
			statusOutrosJogadores[i][statusAux].setPosition(posicoesOutrosJogadores(i, blocoOutrosJogadores[i][numPecasJogadores[i]]).x,posicoesOutrosJogadores(i, blocoOutrosJogadores[i][numPecasJogadores[i]]).y);
			blocoOutrosJogadores[i][numPecasJogadores[i]].draw(batch);
			statusOutrosJogadores[i][statusAux].draw(batch);
		}
		placarAdversario.draw(batch);
		placarMeu.draw(batch);
		PontuacaoDuplas[0][score[0]].draw(batch);
		PontuacaoDuplas[1][score[1]].draw(batch);


		if(estado == Estado.SEGURANDO_PECA && getGameAreaBounds().contains(pecaSelecionada.getBounds()))
			pecaSelecionada.getSprite().setScale(0.5f);
		updateTouchInput();
		boolean trava = false;
		for(boolean b: statusJogadores)
			if(!b)trava=true;
		if(!trava)
			processaMovPecas(batch);
		
		blocoMao.draw(batch);
		for(PecaGUI peca : mao){
			if(peca != null){
				peca.getSprite().draw(batch);
				if(peca != pecaSelecionada || getGameAreaBounds().contains(pecaSelecionada.getBounds()))
					peca.getSprite().setScale(1);
			}
		}
		float posicoesJogadores[][] = new float[3][2];
		float distanciaDaMargem = 30;
		posicoesJogadores[0][0] = (float) (1-((fonte.getMultiLineBounds(nomes[0]).width+distanciaDaMargem)/Gdx.graphics.getWidth()))*Gdx.graphics.getWidth();
		posicoesJogadores[0][1] = Gdx.graphics.getHeight()/2;

		posicoesJogadores[1][0] = Gdx.graphics.getWidth()/2;
		posicoesJogadores[1][1] = (float) (1-((fonte.getMultiLineBounds(nomes[1]).height+distanciaDaMargem)/Gdx.graphics.getHeight()))*Gdx.graphics.getHeight();

		posicoesJogadores[2][0] = distanciaDaMargem;
		posicoesJogadores[2][1] = Gdx.graphics.getHeight()/2;
		fonte.setScale(0.7f);
		for(int i=0; i<3; i++){	
			fonte.draw(batch, nomes[i], posicaoNome(i).x, posicaoNome(i).y);
			//			fonte.draw(batch, ""+statusJogadores[i], posicoesJogadores[i][0], posicoesJogadores[i][1]-fonte.getMultiLineBounds(nomes[i]).height);
			//			fonte.draw(batch, ""+numPecasJogadores[i], posicoesJogadores[i][0], posicoesJogadores[i][1]-2*fonte.getMultiLineBounds(nomes[i]).height);
		}
		fonte.setScale(1.4f);
		if(msgJogadorDaVez && !trava){
			cronometroMsg += Gdx.graphics.getDeltaTime();
			if(cronometroMsg<3){
				batch.draw(escurece, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				fonte.draw(batch, Mensagem, (Gdx.graphics.getWidth()-fonte.getMultiLineBounds(jogadorDaVez+" Vez de").width)/2, (Gdx.graphics.getHeight()-fonte.getMultiLineBounds(jogadorDaVez+" Vez de").height)/2);
			}
			else{
				cronometroMsg = 0;
				msgJogadorDaVez = false;
			}
		}
		//		fonte.draw(batch, score[0]+" X "+score[1], 20, Gdx.graphics.getHeight());
		if(trava){
			batch.draw(escurece, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			fonte.drawMultiLine(batch, "Tem jogador off\nAguarde.", (Gdx.graphics.getWidth()-fonte.getMultiLineBounds("Tem jogador off\nAguarde.").width)/2, (Gdx.graphics.getHeight())/2);

		}		
	}
	private synchronized void processaMovPecas(SpriteBatch batch) {

		fonte.setScale(0.55f);
		if(estado == Estado.MINHA_VEZ)
		{
			fonte.draw(batch, "SUA VEZ!", 50, 100);
			if(isTapped())
			{
				int indexAux = 0;
				int index = 0;
				PecaGUI pecaTocada = null;
				for (PecaGUI peca : mao) {
					if(toqueNaPeca(peca))
					{
						pecaTocada = peca;
						index = indexAux;
					}
					indexAux++;
				}
				if(pecaTocada != null)
				{
					mao[index] = mao[mao.length-1];
					mao[mao.length-1] = pecaTocada;
					pecaSelecionada = pecaTocada;
					estado = Estado.SEGURANDO_PECA;
					pecaSelecionada.pegar();
				}
			}
		}
		else if(estado == Estado.SEGURANDO_PECA)
		{
			if(isTouched)
				pecaSelecionada.setPosicao(touch.x, touch.y);
			else 
			{
				if(this.mao.length == 1)
				{
					System.out.println("vai bater");
				}
				if(pecaSelecionada.overlaps(tabuleiro.firstElement()) && pecaSelecionada.merge(tabuleiro.firstElement(), podeGirar(false,pecaSelecionada)))
				{
					pecaSelecionada.getSprite().setColor(Color.RED);
					pecaSelecionada.setScale(0.5f);
					jogo.getConexao().sendMessage(MensagemJogoJogada.createString(pecaSelecionada.getNumeros()[0], pecaSelecionada.getNumeros()[1], false));
					this.estado = Estado.VEZ_DE_OUTRO;
				}
				else if(pecaSelecionada.overlaps(tabuleiro.lastElement()) && pecaSelecionada.merge(tabuleiro.lastElement(), podeGirar(false,pecaSelecionada)))
				{
					pecaSelecionada.getSprite().setColor(Color.RED);
					pecaSelecionada.setScale(0.5f);
					jogo.getConexao().sendMessage(MensagemJogoJogada.createString(pecaSelecionada.getNumeros()[0], pecaSelecionada.getNumeros()[1], true));
					this.estado = Estado.VEZ_DE_OUTRO;
				}
				else
				{
					this.estado = Estado.MINHA_VEZ;
					pecaSelecionada.soltar();
				}
			}

		}else{
			fonte.draw(batch, "VEZ DE "+jogadorDaVez, 30, 100);
		}
	}
	//	private boolean deveDarCurva(PecaGUI peca)
	//	{
	//		return !getGameAreaBounds().contains(peca.getBounds());
	//	}
	boolean isTouched;
	boolean wasTouched;
	public Vector2 touch;
	private void updateTouchInput()
	{
		wasTouched = isTouched;
		isTouched = Gdx.input.isTouched();
		touch = DominoGdx.getToqueTela();
	}
	public boolean isTapped()
	{
		return isTouched && !wasTouched;
	}
	public boolean toqueNaPeca(PecaGUI peca)
	{
		return peca.getBounds().contains(touch.x, touch.y);
	}

	

	public void fimDePartida(boolean b) {

		fonte.setScale(1.4f);
		if(b){
			Mensagem = "VOCES VENCERAM!";
		}else{
			Mensagem = "Voces Perderam.";
		}
	}

	@Override
	public void input(String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void canceled() {
		// TODO Auto-generated method stub

	}

	public void empate() {
		Mensagem = "Empatou.";		
	}
}
