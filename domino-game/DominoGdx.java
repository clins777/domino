package com.cin.domino;

import java.util.ArrayList;
import java.util.Scanner;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.cin.domino.redes.connection.Connection;
import com.cin.domino.redes.connection.MessageListener;
import com.cin.domino.redes.connection.TCPConnection;

public class DominoGdx implements ApplicationListener, GestureListener, MessageListener
{
	//teste
	//boolean teste = false;
	//boolean iniciou = false;
	int cont=0;
	//teste
//	private ArrayList<PecaGUI> pecas = new ArrayList<PecaGUI>();
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private GestureDetector gestureDetector;
	// Stuff for touch
	private PecaGUI pecaTocada = null;
	private boolean isTouching = false;
	private Vector3 iniPos, fimPos;
//	private Teste jogo;
	private PlayerView visaoJogador;
	private BitmapFont fonte;
	private Scanner in;
	private Connection conexao;
	private static int porta = 1001;
	
	@Override
	public void create()
	{
		fonte = new BitmapFont();
		fonte.scale((float)1.3);
		gestureDetector = new GestureDetector(this);
		Gdx.input.setInputProcessor(gestureDetector);
		Texture.setEnforcePotImages(false);

		visaoJogador = new PlayerView("", -1);
		for(int i=0; i<7; i++)
		{
			for(int j=0; j<=i;j++)
			{
				visaoJogador.getTodasPecas()[i][j].setScale((float)0.6);
			}
		}
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 1250, 750);
		batch = new SpriteBatch();
		
		System.out.println("digite o ip");
		String ip = in.nextLine();
		conexao = new TCPConnection(ip, "servidor", porta, this);
		conexao.start();
	}

	@Override
	public void dispose()
	{
		for(PecaGUI peca : visaoJogador.getMao())
		{
			peca.getSprite().getTexture().dispose();
		}
		batch.dispose();
	}

	@Override
	public void render()
	{		
		Gdx.gl.glClearColor(0, 0, 0.5f, 0.5f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
		if(Gdx.input.isTouched())
		{
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);
			if(isTouching)
			{
				pecaTocada.getSprite().setX(touchPos.x - pecaTocada.getSprite().getWidth() / 2);
				pecaTocada.getSprite().setY(touchPos.y - pecaTocada.getSprite().getHeight() / 2);
			} else
			{
				pecaTocada = grabStone(touchPos);
				if(pecaTocada != null)
				{
					visaoJogador.getMao().remove(pecaTocada);
					visaoJogador.getMao().add(pecaTocada);
					pecaTocada.setScale((float)1.4);
					System.out.println("Got a stone");
					isTouching = true;
					iniPos = new Vector3(pecaTocada.getSprite().getX(), pecaTocada.getSprite().getY(), 0);
				}
				
			}
		} else
		{
			isTouching = false;
			boolean deuMerge = false;
			if(pecaTocada != null)
			{
				pecaTocada.setScale((float)0.4);
				pecaTocada.setPosicao(pecaTocada.getPosicao());
				fimPos = new Vector3(pecaTocada.getSprite().getX(), pecaTocada.getSprite().getY(), 0);
				int i = 0;
				while(i < visaoJogador.getTabuleiro().size())
				{
					PecaGUI lol = visaoJogador.getTabuleiro().get(i);					
					if(lol.overlaps(pecaTocada))
					{
						pecaTocada.setScale((float)1);
						lol.setScale((float)1);
						System.out.println(i+"   "+lol.toString());
						System.out.println("   "+pecaTocada.toString());
						int[] pecaTocada1 = pecaTocada.getNumeros(), pecaTocada2 = lol.getNumeros();
						boolean darCurva=false;
						
						if((pecaTocada1[0] == pecaTocada2[0] || pecaTocada1[0] == pecaTocada2[1] || pecaTocada1[1] == pecaTocada2[0] || pecaTocada1[1] == pecaTocada2[1]) && pecaTocada.merge(lol, darCurva))
						{
							int lado;
							if(i == 0)
								lado = 0;
							else
								lado = 1;
							conexao.sendMessage(visaoJogador.codificar(pecaTocada, lado));
							cont++;
							System.out.println("entrou!");
							System.out.println(lol.toString()+ "  " + lol.getSprite().toString());
							System.out.println(pecaTocada.toString() + "  " + pecaTocada.getSprite().toString());
							deuMerge = true;
						} else
						{
							if(!lol.isFixa()){
								
									lol.setScale((float)1.15);
							}
							pecaTocada.getSprite().setPosition(iniPos.x, iniPos.y);
						}
					}
					i++;
				}
				if(!deuMerge){ 
					
						pecaTocada.setScale((float)1.15);					
						pecaTocada.getSprite().setPosition(iniPos.x, iniPos.y);
					// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
				}
				
				pecaTocada = null;
			}
		}
		batch.begin();
		fonte.draw(batch, visaoJogador.eu.getNome() + "        ID = ", 700, 40);

		if(visaoJogador.tabulerioIniciado){
			for(PecaGUI peca : visaoJogador.getTabuleiro())
			{
				peca.setScale(1);
				//System.out.println("desenha");
				peca.draw(batch);
			}
		}
		for(PecaGUI peca : visaoJogador.getMao())
		{
			//System.out.println("desenha");
			peca.draw(batch);
		}
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	private PecaGUI grabStone(Vector3 touchPos)
	{
		Rectangle rec;
		for(int i = visaoJogador.getMao().size()-1; i>=0; i--)
		{
			PecaGUI peca = visaoJogador.getMao().get(i);
			rec = peca.getSprite().getBoundingRectangle();
			if(rec.x < touchPos.x && (rec.x + rec.width) > touchPos.x && rec.y < touchPos.y && (rec.y + rec.height) > touchPos.y && !peca.isFixa())
			{
				return peca;
			}
		}
		return null;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
/////////////////////////////////////////////////////////////////VVVVVVVVVVVVVVVVVV
		if(pecaTocada != null){
			System.out.println("rodou  " + pecaTocada.toString());
			pecaTocada.setPosicao((pecaTocada.getPosicao()+1)%4);
		}
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void receiveMessage(String message, Connection connection) {
		visaoJogador.decodificar(message);
	}

	@Override
	public void connectionFail(Connection connection) {
		System.out.println("erro de conexao");
		
	}
}
